import { createBrowserHistory } from 'history';

export default createBrowserHistory({
    basename: 'Vedi/#',
    getUserConfirmation(message, callback) {
      callback(true)
      // Show some custom dialog to the user and call
      // callback(true) to continue the transiton, or
      // callback(false) to abort it.
    }
  });