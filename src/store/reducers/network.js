import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    error: null,
    loading1: false,
    network: [],
};

const networkFetchStart = ( state, action ) => {
    return updateObject( state, { error: null, loading1: true } );
};

const networkFetchSuccess = (state, action) => {
    return updateObject( state, { 
        network: action.users,
        error: null,
        loading1: false,
     } );
};

const networkFetchFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading1: false
    });
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.MY_NETWORK_FETCH_START: return networkFetchStart(state, action);
        case actionTypes.MY_NETWORK_FETCH_SUCCESS: return networkFetchSuccess(state, action);
        case actionTypes.MY_NETWORK_FETCH_FAIL: return networkFetchFail(state, action);
        default:
            return state;
    }
};

export default reducer;