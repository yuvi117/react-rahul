import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";
import { routes } from "../../shared/constants";
const initialState = {
  token: false,
  userId: null,
  error: null,
  loading: false,
  socket: null,
  authRedirectPath: "/",
  currentRoute: null,
  notifications: {
    home: 0,
    network: 0,
  },
  componentsVisibility: {
    navBar: true,
    footer: true
  },
  surveys: {
    1: {
      question: "1. What industry do you work in?",
      value: "industry",
      options: [
        {
          id: 1,
          imageName: "microscope.png",
          name: "microscope"
        },
        {
          id: 2,
          imageName: "builder.png",
          name: "builder"
        },
        {
          id: 3,
          imageName: "atom.png",
          name: "atom"
        },
        {
          id: 4,
          imageName: "speech-bubble.png",
          name: "speech-bubble"
        },
        {
          id: 5,
          imageName: "work.png",
          name: "work"
        },
        {
          id: 6,
          imageName: "waiter.png",
          name: "waiter"
        }
      ]
    },
    2: {
      question: "2. You recently got a new pet. Which animal did you choose?",
      value: "recentPet",
      options: [
        {
          id: 1,
          imageName: "snake.png",
          name: "snake"
        },
        {
          id: 2,
          imageName: "dog.png",
          name: "dog"
        },
        {
          id: 3,
          imageName: "rabbit.png",
          name: "rabbit"
        },
        {
          id: 4,
          imageName: "cat.png",
          name: "cat"
        },
        {
          id: 5,
          imageName: "tortoise.png",
          name: "tortoise"
        },
        {
          id: 6,
          imageName: "horse.png",
          name: "horse"
        }
      ]
    },
    3: {
      question: "3. What you like to do on sunday ? ",
      value: "spendSunday",
      options: [
        {
          id: 1,
          imageName: "game.png",
          name: "game"
        },
        {
          id: 2,
          imageName: "headphones.png",
          name: "headphones"
        },
        {
          id: 3,
          imageName: "sunset.png",
          name: "sunset"
        },
        {
          id: 4,
          imageName: "reading.png",
          name: "reading"
        },
        {
          id: 5,
          imageName: "bicycle.png",
          name: "bicycle"
        },
        {
          id: 6,
          imageName: "adventurer.png",
          name: "adventurer"
        }
      ]
    },
    4: {
      question: "4. What do you like to do on rainy days?",
      value: "spendRainyDays",
      options: [
        {
          id: 1,
          imageName: "open-book.png",
          name: "open-book"
        },
        {
          id: 2,
          imageName: "umbrella.png",
          name: "umbrella"
        },
        {
          id: 3,
          imageName: "coffee-cup.png",
          name: "coffee-cup"
        },
        {
          id: 4,
          imageName: "weightlifter.png",
          name: "weightlifter"
        },
        {
          id: 5,
          imageName: "monitor.png",
          name: "monitor"
        },
        {
          id: 6,
          imageName: "sofa.png",
          name: "sofa"
        }
      ]
    },
    5: {
      question: "5. What does your ideal data look like?",
      value: "idealDate",
      options: [
        {
          id: 1,
          imageName: "champagne-glass.png",
          name: "champagne-glass"
        },
        {
          id: 2,
          imageName: "man-travelling.png",
          name: "man-travelling"
        },
        {
          id: 3,
          imageName: "cinema.png",
          name: "cinema"
        },
        {
          id: 4,
          imageName: "piano.png",
          name: "piano"
        },
        {
          id: 5,
          imageName: "yatch.png",
          name: "yatch"
        },
        {
          id: 6,
          imageName: "bowling-ball.png",
          name: "bowling-ball"
        }
      ]
    }
  }
};

const authStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const authSuccess = (state, action) => {
  return updateObject(state, {
    token: action.idToken,
    userId: action.userId,
    error: null,
    loading: false,
    currentRoute: action.route || state.currentRoute
  });
};

const authUploadImageStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const authUploadImageSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false
  });
};

const authUploadImageFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const authForgotPasswordSucess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    authRedirectPath: "/Login"
  });
};

const authFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const authLogout = (state, action) => {
  return updateObject(state, {
    token: null,
    userId: null,
    currentRoute: routes.LOGIN,
    socket: null,
    notifications: initialState.notifications
  });
};

const sideDrawerStatusChange = (state, action) => {
  return updateObject(state, {
    sideDrawer: action.status
  });
};

const currentRouteChange = (state, action) => {
  return updateObject(state, {
    currentRoute: action.route
  });
};

const redirectPathChange = (state, action) => {
  return updateObject(state, {
    authRedirectPath: action.path
  });
};

const changeComponentVisibility = (state, action) => {
  return updateObject(state, {
    componentsVisibility: updateObject(
      state.componentsVisibility,
      action.status
    )
  });
};

const changeNotificationStatus = (state, action) => {
  return updateObject(state, {
    notifications: updateObject(
      state.notification,
      action.status
    )
  });
};

const addSocket = (state, action) => {
  return updateObject(state, {
    socket: action.socket
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return authStart(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.AUTH_UPLOAD_IMAGE_START:
      return authUploadImageStart(state, action);
    case actionTypes.AUTH_UPLOAD_IMAGE_SUCCESS:
      return authUploadImageSuccess(state, action);
    case actionTypes.AUTH_UPLOAD_IMAGE_FAIL:
      return authUploadImageFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    case actionTypes.CHANGE_SIDE_DRAWER_STATUS:
      return sideDrawerStatusChange(state, action);
    case actionTypes.CHANGE_ROUTE:
      return currentRouteChange(state, action);
    case actionTypes.SENT_FORGOT_PASSWORD_MAIL_SUCCESS:
      return authForgotPasswordSucess(state, action);
    case actionTypes.CHANGE_REDIRECT_PATH:
      return redirectPathChange(state, action);
    case actionTypes.CHANGE_COMPONENT_VISIBILITY:
      return changeComponentVisibility(state, action);
    case actionTypes.CHANGE_NOTIFICATION_STATUS:
      return changeNotificationStatus(state, action);
    case actionTypes.ADD_SOCKET:
      return addSocket(state, action);
    default:
      return state;
  }
};

export default reducer;
