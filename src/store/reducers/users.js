import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    error: null,
    loading: false,
    similarIntersetUsers: [],
};

const fetchStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const fetchSuccess = (state, action) => {
    return updateObject( state, { 
        similarIntersetUsers: action.users,
        error: null,
        loading: false,
     } );
};

const fetchFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};

const actionStart = ( state, action ) => {
    return updateObject( state, { error: null, loading: true } );
};

const actionSuccess = (state, action) => {
    return updateObject( state, { 
        error: null,
        loading: false,
     } );
};

const actionFail = (state, action) => {
    return updateObject( state, {
        error: action.error,
        loading: false
    });
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SIMILAR_INTEREST_USERS_FETCH_START: return fetchStart(state, action);
        case actionTypes.SIMILAR_INTEREST_USERS_FETCH_SUCCESS: return fetchSuccess(state, action);
        case actionTypes.SIMILAR_INTEREST_USERS_FETCH_FAIL: return fetchFail(state, action);
        case actionTypes.TAKE_ACTION_START: return actionStart(state, action);
        case actionTypes.TAKE_ACTION_SUCCESS: return actionSuccess(state, action);
        case actionTypes.TAKE_ACTION_FAIL: return actionFail(state, action);
        default:
            return state;
    }
};

export default reducer;