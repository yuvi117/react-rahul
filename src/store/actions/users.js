
import * as actionTypes from './actionTypes';
import { axiosInstance as axios } from '../../axios-orders';
import { encrypt } from '../../shared/utility';


export const usersWithSimilarInterestFetchStart = () => {
    return {
        type: actionTypes.SIMILAR_INTEREST_USERS_FETCH_START
    };
};

export const usersWithSimilarInterestFetchSuccess = (users) => {
    return {
        type: actionTypes.SIMILAR_INTEREST_USERS_FETCH_SUCCESS,
        users,
    };
};

export const usersWithSimilarInterestFetchFail = (error) => {
    return {
        type: actionTypes.SIMILAR_INTEREST_USERS_FETCH_FAIL,
        error: error
    };
};

export const takeActionStart = () => {
    return {
        type: actionTypes.TAKE_ACTION_START
    };
};

export const takeActionSuccess = (users, actionType) => {
    return {
        type: actionTypes.TAKE_ACTION_SUCCESS,
        users,
        actionType,
    };
};

export const takeActionFail = (error) => {
    return {
        type: actionTypes.TAKE_ACTION_FAIL,
        error: error
    };
};

export const getusersWithSimilarInterest = (token) => {
    return dispatch => {
        dispatch(usersWithSimilarInterestFetchStart());

        const headers = {
            access_token: token,
        }
        
        let url = `user/similar_interest`;
        axios.get(url, {headers})
            .then(response => {
                
                const data = response.data.data.splice(0, 8)
                dispatch(usersWithSimilarInterestFetchSuccess(data));
                sessionStorage.setItem('usersWithSimilarInterest', JSON.stringify(encrypt(JSON.stringify(data))));

            })
            .catch(err => {
                console.log('errrror', err);
                dispatch(usersWithSimilarInterestFetchFail(err));
            });
    };
};

export const takeAction = (token, actionData, userFetch) => {
    return dispatch => {
        dispatch(takeActionStart());

        const headers = {
            access_token: token,
        }
        
        let url = `user/take_action`;
        axios.post(url, actionData, {headers})
            .then(response => {
                sessionStorage.setItem('usersWithSimilarInterest', JSON.stringify(encrypt(response.data.data)));
                dispatch(takeActionSuccess(response.data.data));
                dispatch(getusersWithSimilarInterest(token));
            })
            .catch(err => { 
                console.log('errrror', err);
                dispatch(takeActionFail(err));
            });
    };
};