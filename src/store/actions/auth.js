// import axios from 'axios'

import * as actionTypes from "./actionTypes";
import Cookies from "universal-cookie";
import { encrypt, decrypt, getGMTTime } from "../../shared/utility";
import { axiosInstance as axios } from "../../axios-orders";

const cookies = new Cookies();

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = (token, userId, route) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    idToken: token,
    userId: userId,
    route
  };
};

export const sendForgotPasswordMailSuccess = () => {
  return {
    type: actionTypes.SENT_FORGOT_PASSWORD_MAIL_SUCCESS,
  };
};

export const changeRedirectPath = path => {
  return {
    type: actionTypes.CHANGE_REDIRECT_PATH,
    path
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const authUploadImageStart = () => {
  return {
    type: actionTypes.AUTH_UPLOAD_IMAGE_START,
  };
};

export const authUploadImageSuccess = (token, userId, route) => {
  return {
    type: actionTypes.AUTH_UPLOAD_IMAGE_SUCCESS,
    route
  };
};

export const authUploadImageFail = error => {
  return {
    type: actionTypes.AUTH_UPLOAD_IMAGE_FAIL,
    error: error
  };
};

export const logout = () => {
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const noChanges = () => {
  return {
    type: actionTypes.DEFAULT
  };
};

export const changeCurrentRoute = route => {
  return {
    type: actionTypes.CHANGE_ROUTE,
    route
  };
};

export const changeComponentVisibility = status => {
  return {
    type: actionTypes.CHANGE_COMPONENT_VISIBILITY,
    status
  };
};

export const changeNotificationStatus = status => {
  return {
    type: actionTypes.CHANGE_NOTIFICATION_STATUS,
    status
  };
};

export const addUserSocket = socket => {
  return {
    type: actionTypes.ADD_SOCKET,
    socket
  };
};

export const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};

export const auth = (email, password, route = 1) => {
  return async dispatch => {
    try {
      dispatch(authStart());
      const authData = {
        userEmail: email,
        userPassword: password
      };
      let url = `auth/login`;
      axios.post(url, authData).then((response, err) => {
        if (response) {
          const date = getGMTTime(7);
          cookies.set(
            "token",
            JSON.stringify(encrypt(response.data.data.accessToken)),
            { expires: date }
          );
          cookies.set(
            "userId",
            JSON.stringify(encrypt(response.data.data.responseData._id)),
            { expires: date }
          );
          dispatch(
            authSuccess(
              response.data.data.accessToken,
              response.data.data.responseData._id,
              route
            )
          );
        } else {
          dispatch(authFail("No response"));
        }
      });
    } catch (e) {
      dispatch(authFail(e));
      console.log("error", e);
    }
  };
};

export const authSignup = (signupData, route = 1) => {
  return dispatch => {
    dispatch(authStart());
    console.log("signupData", signupData);
    let url = `auth/register`;
    axios
      .post(url, signupData)
      .then(response => {
        console.log("response",response)
        if (response) {
          const date = getGMTTime(7);
          cookies.set(
            "token",
            JSON.stringify(encrypt(response.data.data.accessToken)),
            { expires: date }
          );
          cookies.set(
            "userId",
            JSON.stringify(encrypt(response.data.data.data._id)),
            { expires: date }
          );
          dispatch(
            authSuccess(
              response.data.data.accessToken,
              response.data.data.data._id,
              route
            )
          );
        } else {
          dispatch(authFail("No response"));
        }
      })
      .catch(err => {
        console.log("errrror", err.respone);
        dispatch(authFail(err));
      });
  };
};

export const authForgotPassword = (email, route = 1) => {
  return dispatch => {
    dispatch(authStart());
    let url = `user/forgot_password`;
    axios
      .post(url, { userEmail: email })
      .then(response => {
        dispatch(sendForgotPasswordMailSuccess());
      })
      .catch(err => {
        console.log("errrror", err.respone);
        dispatch(authFail(err));
      });
  };
};

export const authLogout = (token) => {
  return dispatch => {
    if (token) {
      const headers = {
        access_token: token
      };
      let url = `auth/logout`;
      axios.get(url, { headers }).then((response) => {
        if (response) {
          cookies.remove('token');
          cookies.remove('userId');
          dispatch(logout());
        } else {
          dispatch(noChanges());
        }
      })
    } else {
      dispatch(logout());
    }
  };
};

export const authCheckState = () => {
  return dispatch => {
    let token = cookies.get("token");
    if (!token) {
      dispatch(logout(token));
    } else {
      let userId = cookies.get("userId");
      userId = decrypt(userId);
      token = decrypt(token);
      dispatch(authSuccess(token, userId));
    }
  };
};

export const authUploadImage = data => {
  return dispatch => {
    dispatch(authUploadImageStart());
    const formData = new FormData();
    data.filter((image, index) => {
      if (image !== null) {
        formData.append("files", image[0]);
      }

      return image !== null;
    });

    let url = `util/upload_media`;
    axios
      .post(url, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(response => {
        const data = response.data.data.files;
        sessionStorage.setItem(
          "userSignupImages",
          JSON.stringify(encrypt(JSON.stringify(data)))
        );
        dispatch(authUploadImageSuccess());
      })
      .catch(err => {
        console.log("errrror", err);
        dispatch(authUploadImageFail(err));
      });
  };
};

export const changeRoute = route => {
  return dispatch => {
    dispatch(changeCurrentRoute(route));
  };
};

export const changeAuthRedirect = path => {
  return dispatch => {
    dispatch(changeRedirectPath(path));
  };
};

export const changeVisibility = status => {
  return dispatch => {
    dispatch(changeComponentVisibility(status));
  };
};

export const changeNotification = status => {
  return dispatch => {
    dispatch(changeNotificationStatus(status));
  };
};

export const socketConnected = socket => {
  return dispatch => {
    dispatch(addUserSocket(socket));
  };
};
