
import * as actionTypes from './actionTypes';
import { axiosInstance as axios } from '../../axios-orders';


export const myNetworkFetchStart = () => {
    return {
        type: actionTypes.MY_NETWORK_FETCH_START
    };
};

export const myNetworkFetchSuccess = (users, actionType) => {
    return {
        type: actionTypes.MY_NETWORK_FETCH_SUCCESS,
        users,
        actionType,
    };
};

export const myNetworkFetchFail = (error) => {
    return {
        type: actionTypes.MY_NETWORK_FETCH_FAIL,
        error: error
    };
};

export const takeActionStart = () => {
    return {
        type: actionTypes.TAKE_ACTION_START
    };
};

export const getMyNetwork = (token) => {
    return dispatch => {
        dispatch(myNetworkFetchStart());

        const headers = {
            access_token: token,
        }
        
        let url = `user/get/my_network`;
        axios.get(url, {headers})
            .then(response => {
                
                console.log('users', response.data.data)
                dispatch(myNetworkFetchSuccess(response.data.data));

            })
            .catch(err => {
                console.log('errrror', err);
                dispatch(myNetworkFetchFail(err));
            });
    };
};