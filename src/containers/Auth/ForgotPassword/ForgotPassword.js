import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, NavLink } from 'react-router-dom'
import axios from 'axios';

import Input from '../../../components/UI/Input/Login/InputLogin'
import Spinner from '../../../components/UI/Spinner/Spinner'
import classes from './ForgotPassword.css'
import * as actions from '../../../store/actions/index'
import { updateObject, checkValidity } from '../../../shared/utility'
import { routes } from '../../../shared/constants';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';

class ForgotPassword extends Component {
  state = {
    controls: {
        email: {
            elementType: 'input',
            message: 'This is required',
            class: 'rs1WrapInput100',
            Iconclass: 'fa fa-envelope',
            elementConfig: {
              type: 'email',
              placeholder: 'Enter username / Email'
            },
            value: '',
            validation: {
              required: true,
              isEmail: true
            },
            valid: false,
            touched: false
          }
    },
    formIsValid: false,
    validationMessage: '',
  }


  componentWillUnmount () {
    this.props.changeRedirectPath('/')
  }

  inputChangedHandler = (event, controlName) => {
    const validationData = checkValidity(
      event.target.value,
      this.state.controls[controlName].validation,
      controlName
    )
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event.target.value,
        valid: validationData.isValid,
        touched: true,
        message: validationData.message
      })
    })
    let formIsValid = true
    for (let inputIdentifier in updatedControls) {
      formIsValid = updatedControls[inputIdentifier].valid && formIsValid
    }
    this.setState({
      controls: updatedControls,
      formIsValid: formIsValid,
      validationMessage: validationData.isValid ? null : validationData.message
    })
  }

  submitHandler = event => {
    event.preventDefault()
    if (!this.state.formIsValid) {
      const updatedControls = {
        ...this.state.controls
      }
      for (let inputIdentifier in updatedControls) {
        updatedControls[inputIdentifier].touched = true
      }
      this.setState({
        controls: updatedControls
      })
    } else {
      this.props.onSendForgetMail(
        this.state.controls.email.value
      )
      this.setState({})
    }
  }

  render () {
    const formElementsArray = []
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key]
      })
    }

    let errorMessage = null

    if (this.props.error) {
      errorMessage = <p>{this.props.error.message}</p>
    }

    let authRedirect = null
    if (this.props.authRedirectPath !== '/') {
      authRedirect = <Redirect to='/Login' />
    }

    return (
      <section
        id='login_sec'
        className={`${classes.login_form} ${classes.space_100}`}
      >
        {this.props.loading ? (
          <Spinner />
        ) : (
          <div className='container'>
            <h1 className={`text-center ${classes.space_60}`}>Forgot Password</h1>
            <div className={`${classes.form_style} ${classes.login}`}>
              {authRedirect}
              <form onSubmit={this.submitHandler}>
              <p>We will send you reset password Link on email</p>
              <div
                          className={`${classes.formGroup} ${
                            classes['m-b-20']
                          } ${classes.wrapInput100} ${
                            !this.state.controls.email.valid &&
                            this.state.controls.email.validation &&
                            this.state.controls.email.touched
                              ? classes.alertValidate
                              : null
                          }`}
                          datavalidate={this.state.controls.email.message}
                        >
                          <input
                            type='text'
                            className={classes.formControl}
                            placeholder='Enter Email'
                            value={this.state.controls.email.value}
                            onChange={event =>
                              this.inputChangedHandler(event, 'email')
                            }
                          />
                          <i className='fa fa-envelope' />
                        </div>
​
          
              <div className={`${classes.btn_work} ${classes.login_btn} text-center`}>
                <button type="submit" className={`${classes.btn} btn-default`}>Send</button>
            </div>
              </form>
            </div>
          </div>
        )}
      </section>
    )
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSendForgetMail: (email) => dispatch(actions.authForgotPassword(email)),
    changeRedirectPath: (path) => dispatch(actions.changeAuthRedirect(path))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(ForgotPassword, axios))
