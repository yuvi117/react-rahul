import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect, NavLink } from "react-router-dom";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng
} from "react-places-autocomplete";
import PhoneInput from "react-phone-input-2";
// eslint-disable-next-line
import "!style-loader!css-loader!react-phone-input-2/dist/style.css";
// eslint-disable-next-line
import "!style-loader!css-loader!./../../../components/Forms/ContactForm/ContactForm.css";
import Spinner from "../../../components/UI/Spinner/Spinner";
import classes from "./SignUp.css";
import * as actions from "../../../store/actions/index";
import { updateObject, checkValidity, decrypt } from "../../../shared/utility";
import Aux from "../../../hoc/Auxx/Auxx";
import Survey from "../../../components/Survey/Survey";
import { routes } from "../../../shared/constants";
import withErrorHandler from "../../../hoc/withErrorHandler/withErrorHandler";
import axiosCus, { axiosInstance as axios } from "../../../axios-orders";
import Icon from "../../../components/UI/Icons/Icons";
import AddImageButton from "../../../components/UI/AddImageButton/AddImageButton";
import SignupInput from "../../../components/UI/Input/SignUp/InputSignup";

class SignUp extends Component {
  state = {
    controls: {
      email: {
        type: "text",
        placeholder: "Email",
        field: "email",
        message: "This is required",
        Iconclass: "fa fa-envelope",
        value: "",
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false,
        available: false,
        loading: false
      },
      password: {
        type: "password",
        placeholder: "Password",
        field: "password",
        message: "This is required",
        Iconclass: "fa fa-lock",
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        available: true,
        loading: false
      },
      name: {
        type: "text",
        placeholder: "Name",
        field: "name",
        message: "This is required",
        Iconclass: "fa fa-user",
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        available: true,
        loading: false
      },
      number: {
        type: "text",
        placeholder: "Phone Number",
        field: "number",
        message: "This is required",
        Iconclass: "fa fa-lock",
        value: "",
        countryCode: "",
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
        touched: false,
        available: false,
        loading: false
      },
      location: {
        type: "text",
        placeholder: "Search Places...",
        field: "location",
        message: "This is required",
        Iconclass: "fa fa-lock",
        value: "",
        lat: "",
        lon: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        available: true,
        loading: false
      },
      repeatPassword: {
        type: "password",
        placeholder: "Repeat Password",
        field: "repeatPassword",
        message: "This is required",
        Iconclass: "fa fa-lock",
        value: "",
        validation: {
          required: true,
          compare: true
        },
        valid: false,
        touched: false,
        available: true,
        loading: false
      },
      profilePic: {
        type: "file",
        placeholder: "",
        field: "profilePic",
        message: "This is required",
        value: [null, null, null],
        valid: true,
        touched: false,
        available: true,
        loading: false
      }
    },

    formIsValid: false,
    validationMessage: "",
    submit: false,
    currentSurvey: 1,
    selectedSurveyOptions: {
      1: 0,
      2: 0,
      3: 0,
      4: 0,
      5: 0
    },
    surveyData: {
      industry: "",
      recentPet: "",
      spendSunday: "",
      spendRainyDays: "",
      idealDate: ""
    },
    checkBox: false,
    submitButton: true
  };

  componentDidUpdate() {
    if (
      this.props.error &&
      this.state.controls.profilePic.touched &&
      this.state.submit
    ) {
      this.setState({
        submit: false
      });
    }
  }
  changeSurveyHandler = () => {
    if (this.state.currentSurvey < Object.keys(this.props.surveys).length) {
      this.setState(prevState => {
        return {
          currentSurvey: prevState.currentSurvey + 1
        };
      });
    } else {
      let signupdata = {
        userEmail: this.state.controls.email.value,
        userPassword: this.state.controls.password.value,
        userFullname: this.state.controls.name.value,
        userPhone: this.state.controls.number.value,
        userCountryCode: this.state.controls.number.countryCode,
        userAddress: this.state.controls.location.value,
        lat: this.state.controls.location.lat,
        long: this.state.controls.location.lon,
        ...this.state.surveyData
      };

      if (this.state.controls.profilePic.touched) {
        let images = sessionStorage.getItem("userSignupImages");
        images = decrypt(JSON.parse(images));
        signupdata.userPics = JSON.parse(images).join(",");
        sessionStorage.removeItem("userSignupImages");
      }
      this.props.onAuth(signupdata, routes.HOME);
    }
  };

  accessCurrentLocationHandler = () => {
    if (this.state.checkBox) {
      this.setState(prevState => {
        return { checkBox: !prevState.checkBox };
      });
    } else {
      let formIsValid = true;

      const updatedControls = {
        ...this.state.controls
      };
      const updateState = updatedControls => {
        this.setState(prevState => {
          return {
            controls: updatedControls,
            checkBox: !prevState.checkBox,
            formIsValid: formIsValid,
            submitButton: true
          };
        });
      };
      window.navigator.geolocation.getCurrentPosition(showPosition);
      this.setState({
        submitButton: false
      });
      function showPosition(position) {
        axios
          .get(
            `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=AIzaSyAD5-Eu07svHvRfYtOZyLGchE4KNtzzJEA`
          )
          .then(response => {
            updatedControls["location"].value =
              response.data.results[0].formatted_address;
            updatedControls[
              "location"
            ].lat = position.coords.latitude.toString();
            updatedControls[
              "location"
            ].lon = position.coords.longitude.toString();
            updatedControls["location"].valid = true;
            for (let inputIdentifier in updatedControls) {
              formIsValid =
                updatedControls[inputIdentifier].valid &&
                updatedControls[inputIdentifier].available &&
                !updatedControls[inputIdentifier].loading &&
                formIsValid;
            }
            updateState(updatedControls);
          })
          .catch(err => {
            for (let inputIdentifier in updatedControls) {
              formIsValid =
                updatedControls[inputIdentifier].valid &&
                updatedControls[inputIdentifier].available &&
                !updatedControls[inputIdentifier].loading &&
                formIsValid;
            }
            updateState(updatedControls);
            console.log("errrror", err);
          });
      }
    }
  };

  onSelectSurveyOption = (surveyId, name) => {
    let updatedselectedSurveyOptions = { ...this.state.selectedSurveyOptions };
    updatedselectedSurveyOptions[this.state.currentSurvey] = surveyId;

    let updatedsurveyData = { ...this.state.surveyData };
    updatedsurveyData[
      this.props.surveys[this.state.currentSurvey].value
    ] = name;
    this.setState({
      surveyData: updatedsurveyData,
      selectedSurveyOptions: updatedselectedSurveyOptions
    });
  };

  handleSelect = address => {
    let lat = "";
    let lon = "";
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        lat = latLng.lat;
        lon = latLng.lng;
        const updatedControls = {
          ...this.state.controls
        };
        updatedControls["location"].value = address;
        updatedControls["location"].lat = lat.toString();
        updatedControls["location"].lon = lon.toString();
        this.setState({ controls: updatedControls });
      })
      .catch(error => console.error("Error", error));
  };

  imageSelectedHandler = (e, index) => {
    if (e.target.files && e.target.files.length > 0) {
      let updatedFormData = {
        ...this.state.controls
      };
      updatedFormData.profilePic.value[index] = e.target.files;
      updatedFormData.profilePic.touched = true;
      this.setState({ formData: updatedFormData });
    }
  };

  inputChangedHandler = (event, controlName, extraValues) => {
    const validationData = checkValidity(
      event.target.value,
      this.state.controls[controlName].validation,
      controlName,
      { ...extraValues }
    );
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event.target.value,
        valid: validationData.isValid,
        touched: true,
        message: validationData.message
      })
    });
    if (
      controlName === "password" &&
      this.state.controls.repeatPassword.valid === true &&
      this.state.controls.repeatPassword.value !== event.target.value
    ) {
      updatedControls["repeatPassword"].valid = false;
    }

    if (controlName === "number") {
      updatedControls["number"].countryCode = extraValues.countryCode;
    }

    if (
      extraValues &&
      extraValues.checkAvalability &&
      updatedControls[controlName].valid &&
      this.state.controls.number.value !== event.target.value
    ) {
      updatedControls[controlName].loading = true;
      this.setState({
        controls: updatedControls
      });
      axiosCus
        .post("auth/check/avaibility", extraValues.payload)
        .then(response => {
          updatedControls[controlName].available = true;
          updatedControls[controlName].loading = false;

          let formIsValid = true;
          for (let inputIdentifier in updatedControls) {
            formIsValid =
              updatedControls[inputIdentifier].valid &&
              updatedControls[inputIdentifier].available &&
              !updatedControls[inputIdentifier].loading &&
              formIsValid;
          }
          this.setState({
            controls: updatedControls,
            formIsValid: formIsValid
          });
          this.setState({
            controls: updatedControls
          });
        })
        .catch(err => {
          updatedControls[controlName].available = false;
          updatedControls[controlName].loading = false;
          this.setState({
            controls: updatedControls
          });
          console.log("errrror", err);
        });
    }

    let formIsValid = true;
    for (let inputIdentifier in updatedControls) {
      formIsValid =
        updatedControls[inputIdentifier].valid &&
        updatedControls[inputIdentifier].available &&
        !updatedControls[inputIdentifier].loading &&
        formIsValid;
    }
    this.setState({
      controls: updatedControls,
      formIsValid: formIsValid,
      validationMessage: validationData.isValid ? null : validationData.message
    });
  };

  submitHandler = event => {
    event.preventDefault();
    if (!this.state.formIsValid) {
      const updatedControls = {
        ...this.state.controls
      };
      for (let inputIdentifier in updatedControls) {
        updatedControls[inputIdentifier].touched = true;
      }
      this.setState({
        controls: updatedControls
      });
    } else if (this.state.controls.profilePic.touched) {
      this.props.onUploadImages([...this.state.controls.profilePic.value]);
      this.setState({
        submit: true
      });
    } else {
      this.setState({
        submit: true
      });
    }
  };

  render() {
    // let errorMessage = null;

    // if (this.props.error) {
    //   errorMessage = <p>{this.props.error.message}</p>;
    // }

    let authRedirect = null;
    if (this.props.isAuthenticated) {
      authRedirect = <Redirect to="/Home" />;
    }

    return (
      <Aux>
        {this.props.loading ? (
          <Spinner />
        ) : (
          <Aux>
            {this.state.submit ? (
              <Survey
                changeSurvey={this.changeSurveyHandler}
                surveyData={this.props.surveys[this.state.currentSurvey]}
                surveyLength={Object.keys(this.props.surveys).length}
                currentSurvey={this.state.currentSurvey}
                selectSurveyOption={this.onSelectSurveyOption}
                selectedSurveyOption={
                  this.state.selectedSurveyOptions[this.state.currentSurvey]
                }
              />
            ) : (
              <section
                id="signup_form_sec"
                className={`${classes.signup_form} ${classes.space_100}`}
              >
                <div className="container">
                  <h1 className={`text-center ${classes.space_60}`}>Sign up</h1>
                  <div className={`${classes.form_style} ${classes.login}`}>
                    {authRedirect}
                    <form onSubmit={this.submitHandler}>
                      <div
                        className="partners"
                        style={{
                          marginTop: "5%",
                          marginBottom: "10%",
                          marginRight: "-6%",
                          marginLeft: "10%"
                        }}
                      >
                        <h1
                          style={{
                            marginLeft: "133px",
                            fontWeight: "500",
                            marginBottom: "inherit",
                            WebkitMarginTopCollapse: "discard"
                          }}
                        >
                          Choose a profile
                        </h1>
                        <div 
                        className="row_cus" 
                        style={{
                          display: "-webkit-flex",
                        }}>
                          {this.state.controls.profilePic.value.map(
                            (item, index) => (
                              <AddImageButton
                                key={index}
                                imageIndex={index}
                                file={item}
                                selectedImage={this.imageSelectedHandler}
                              />
                            )
                          )}
                        </div>
                      </div>

                      <div className={classes.partition}>
                        <SignupInput
                          fieldProperties={this.state.controls.name}
                          onValueChange={this.inputChangedHandler}
                          rightSpace
                        />
                        <SignupInput
                          fieldProperties={this.state.controls.email}
                          onValueChange={this.inputChangedHandler}
                        >
                          {this.state.controls.email.loading &&
                          this.state.controls.email.valid ? (
                            <Icon icon="circle Spinner" />
                          ) : this.state.controls.email.available &&
                            this.state.controls.email.valid ? (
                            <Icon icon="check" />
                          ) : !this.state.controls.email.available &&
                            this.state.controls.email.valid ? (
                            <Icon icon="times" />
                          ) : null}
                        </SignupInput>
                      </div>

                      <div className={classes.partition}>
                        <SignupInput
                          fieldProperties={this.state.controls.password}
                          onValueChange={this.inputChangedHandler}
                          rightSpace
                        />

                        <SignupInput
                          fieldProperties={this.state.controls.repeatPassword}
                          onValueChange={this.inputChangedHandler}
                          extraValues={this.state.controls.password.value}
                        />
                      </div>

                      <div className={classes.partition}>
                        <div
                          className={`${classes.formGroup} ${
                            classes.right_space
                          } ${classes["m-b-20"]} ${classes.wrapInput100} ${
                            !this.state.controls.number.valid &&
                            this.state.controls.number.validation &&
                            this.state.controls.number.touched
                              ? classes.alertValidate
                              : null
                          }`}
                          datavalidate={this.state.controls.number.message}
                          style={{ overflow: "visible" }}
                        >
                          <div style={{ position: "relative" }}>
                            <PhoneInput
                              containerStyle={{ marginTop: "2px" }}
                              inputClass={`${classes.formControl}`}
                              placeholder="Phone Number"
                              inputStyle={{
                                boxShadow: "none",
                                border: "none",
                                borderBottom: "1px solid #CACACA",
                                borderRadius: "inherit",
                                width: "257px"
                              }}
                              dropdownStyle={{
                                border: "none",
                                width: "257px",
                                "-webkit-user-select": "text",
                                userSelect: "text"
                              }}
                              value={this.state.controls.number.value}
                              onChange={(value, data) => {
                                return this.inputChangedHandler(
                                  {
                                    target: {
                                      value: value
                                        .replace(/[^0-9]+/g, "")
                                        .slice(
                                          data.dialCode
                                            ? data.dialCode.length
                                            : 0
                                        )
                                    }
                                  },
                                  "number",
                                  {
                                    countryCode: data.dialCode || "",
                                    checkAvalability: true,
                                    payload: {
                                      userPhone: value
                                        .replace(/[^0-9]+/g, "")
                                        .slice(
                                          data.dialCode
                                            ? data.dialCode.length
                                            : 0
                                        )
                                        .toString(),
                                      userCountryCode: data.dialCode
                                        ? data.dialCode.toString()
                                        : "",
                                      status: "2"
                                    }
                                  }
                                );
                              }}
                            />
                            {this.state.controls.number.loading &&
                            this.state.controls.number.valid ? (
                              <Icon icon="circle Spinner" />
                            ) : this.state.controls.number.available &&
                              this.state.controls.number.valid ? (
                              <Icon icon="check" />
                            ) : !this.state.controls.number.available &&
                              this.state.controls.number.valid ? (
                              <Icon icon="times" />
                            ) : null}
                          </div>
                        </div>

                        <div
                          className={`${classes.formGroup} ${
                            classes["m-b-20"]
                          } ${classes.wrapInput100} ${
                            !this.state.controls.location.valid &&
                            this.state.controls.location.validation &&
                            this.state.controls.location.touched
                              ? classes.alertValidate
                              : null
                          }`}
                          datavalidate={this.state.controls.location.message}
                        >
                          <PlacesAutocomplete
                            value={this.state.controls.location.value}
                            onChange={address =>
                              this.inputChangedHandler(
                                { target: { value: address } },
                                "location"
                              )
                            }
                            onSelect={this.handleSelect}
                          >
                            {({
                              getInputProps,
                              suggestions,
                              getSuggestionItemProps,
                              loading
                            }) => (
                              <div>
                                <input
                                  {...getInputProps({
                                    placeholder: "Search Places ...",
                                    className: classes.formControl
                                  })}
                                />
                                <div className={classes.searchDropDown}>
                                  {loading && <div>Loading...</div>}
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                      ? "suggestion-item--active"
                                      : "suggestion-item";
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? {
                                          backgroundColor: "#fafafa",
                                          cursor: "pointer",
                                          border: "2px block black"
                                        }
                                      : {
                                          backgroundColor: "#ffffff",
                                          cursor: "pointer"
                                        };
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            )}
                          </PlacesAutocomplete>
                          {!this.state.submitButton ? (
                            <Icon icon="circle Spinner" />
                          ) : null}
                          <i className="fa fa-map-marker" />
                        </div>
                      </div>

                      <div className={classes.allow_access_location}>
                        <input
                          type="checkbox"
                          // onClick={this.accessCurrentLocationHandler}
                        />
                        &nbsp;&nbsp;Allow access Location
                        <br />
                      </div>

                      <div
                        className={`${classes.btn_work} ${classes.login_btn} text-center`}
                      >
                        <button
                          style={{ color: "#000" }}
                          type="submit"
                          className={`${classes.btn} btn-default`}
                          disabled={!this.state.submitButton}
                        >
                          Submit
                        </button>
                        <br />
                        <span
                          className={`${classes.btn} btn-default ${classes.acc_btn}`}
                        >
                          <NavLink style={{ color: "#000" }} to={"./Login"}>
                            I have an account
                          </NavLink>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
              </section>
            )}
          </Aux>
        )}
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath,
    surveys: state.auth.surveys
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (signupData, route) =>
      dispatch(actions.authSignup(signupData, route)),
    onUploadImages: images => dispatch(actions.authUploadImage(images))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(SignUp, axios));
