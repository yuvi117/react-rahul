import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../../store/actions/index';
// eslint-disable-next-line
import '!style-loader!css-loader!./Logout.css';
import { routes } from '../../../shared/constants';

class Logout extends Component {

    redirectToLoginHandler = () => {
        this.props.logout(null, null);
        this.props.onLogout(this.props.token);
        this.props.socket.disconnect();
        <Redirect to="/login"/>
    }

    render () {
        return (
            <div className={this.props.showPopup ? "cd-popup is-visible" : "cd-popup"} role="alert">
	<div className="cd-popup-container">
		<p>Are you sure you want to Logout?</p>
		<ul className="cd-buttons" style={{listStyle: "none"}}>
			<li onClick={this.redirectToLoginHandler} style={{cursor: "pointer"}}><span>Yes</span></li>
			<li id='' onClick={this.props.closePopup} style={{cursor: "pointer"}}><span>No</span></li>
		</ul>
		<a className="cd-popup-close img-replace" onClick={this.props.closePopup} style={{cursor: "pointer"}}><span>Close</span></a>
	</div> 
</div> 
)
    }
}

const mapStateToProps = state => {
    return {
      isAuthenticated: state.auth.token !== null,
      token: state.auth.token,
      socket: state.auth.socket,
    };
  };

const mapDispatchToProps = dispatch => {
    return {
        onLogout: (token) => dispatch(actions.logout(token)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);