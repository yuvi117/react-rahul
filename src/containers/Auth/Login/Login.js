import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, NavLink } from 'react-router-dom'

import Input from '../../../components/UI/Input/Login/InputLogin'
import Spinner from '../../../components/UI/Spinner/Spinner'
import classes from './Login.css'
import * as actions from '../../../store/actions/index'
import { updateObject, checkValidity } from '../../../shared/utility'
import { routes } from '../../../shared/constants';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import { axiosInstance as axios } from '../../../axios-orders';

class Login extends Component {
  state = {
    controls: {
      email: {
        elementType: 'input',
        message: 'Email is required',
        class: 'rs1WrapInput100',
        Iconclass: 'fa fa-envelope',
        elementConfig: {
          type: 'email',
          placeholder: 'Enter username / Email'
        },
        value: '',
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: 'input',
        message: 'Password is required',
        class: 'rs2WrapInput100',
        Iconclass: 'fa fa-lock',
        elementConfig: {
          type: 'password',
          placeholder: 'Enter Password'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      }
    },
    formIsValid: false,
    validationMessage: ''
  }


  inputChangedHandler = (event, controlName) => {
    const validationData = checkValidity(
      event.target.value,
      this.state.controls[controlName].validation,
      controlName
    )
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event.target.value,
        valid: validationData.isValid,
        touched: true,
        message: validationData.message
      })
    })
    let formIsValid = true
    for (let inputIdentifier in updatedControls) {
      formIsValid = updatedControls[inputIdentifier].valid && formIsValid
    }
    this.setState({
      controls: updatedControls,
      formIsValid: formIsValid,
      validationMessage: validationData.isValid ? null : validationData.message
    })
  }

  submitHandler = event => {
    event.preventDefault()
    if (!this.state.formIsValid) {
      const updatedControls = {
        ...this.state.controls
      }
      for (let inputIdentifier in updatedControls) {
        updatedControls[inputIdentifier].touched = true
      }
      this.setState({
        controls: updatedControls
      })
    } else {
      this.props.onAuth(
        this.state.controls.email.value,
        this.state.controls.password.value,
        routes.HOME,
      )
    }
  }

  render () {
    const formElementsArray = []
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key]
      })
    }

    let form = formElementsArray.map(formElement => (
      <Input
        key={formElement.id}
        classN={classes.input100}
        IconClass={formElement.config.Iconclass}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        divClass={formElement.config.class}
        message={formElement.config.message}
        changed={event => this.inputChangedHandler(event, formElement.id)}
      />
    ))

    if (this.props.loading) {
      form = <Spinner />
    }

    let errorMessage = null

    if (this.props.error) {
      errorMessage = <p>{this.props.error.message}</p>
    }

    let authRedirect = null
    if (this.props.isAuthenticated) {
      authRedirect = <Redirect to='/Home' />
    }

    return (
      <section
        id='login_sec'
        className={`${classes.login_form} ${classes.space_100}`}
      >
        {this.props.loading ? (
          <Spinner />
        ) : (
          <div className='container'>
            <h1 className={`text-center ${classes.space_60}`}>Login</h1>
            <div className={`${classes.form_style} ${classes.login}`}>
              {authRedirect}
              <form onSubmit={this.submitHandler}>
                {form}
                <div className={classes.forgot_pwd}>
                <NavLink to='./forgetPassword' style={{textDecoration: "none"}}>
                  Forgot Password
                  </NavLink>
                </div>

                <div
                  className={`${classes.btn_work} ${
                    classes.login_btn
                  } text-center`}
                >
                  <button
                    type='submit'
                    className={`${classes.btn} btn-default`}
                  >
                    Login
                  </button>
                </div>

                <div className={classes.dont_have_acc}>
                  <p>
                    Don’t have an account?{' '}
                    <NavLink to={'./SignUp'} style={{ color: '#bd9130' }}>
                    
                      Sign up here
                    </NavLink>
                  </p>
                </div>
              </form>
            </div>
          </div>
        )}
      </section>
    )
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password, route) => dispatch(actions.auth(email, password, route))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(Login, axios))
