import React, { Component } from 'react'
import { connect } from 'react-redux'

// eslint-disable-next-line
import '!style-loader!css-loader!./CelesteHiring.css'
import * as actions from '../../../store/actions/index'
import { routes } from '../../../shared/constants';

class CelesteHiring extends Component {

  componentDidMount () {
    this.props.onRouteChange(routes.CELESTE_HIRING);
  }
  render () {
    return (
        <section className="upload_celeste space_100">
        <div className="container">
          <div className="upload_buttons text-center">
            <button type="submit" onClick={() => this.props.history.push('/celesteHiring/celeste_form')} className="btn btn-default">Upload a Venue</button>
            <button type="submit" className="btn btn-default">I am looking to serve</button>
       </div>
       </div>
      </section>
      )
    }
  }
  
  const mapStateToProps = state => {
      return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        surveys: state.auth.surveys
      }
    }
    
    const mapDispatchToProps = dispatch => {
      return {
        onAuth: (email, password) => dispatch(actions.auth(email, password)),
        onRouteChange: route => dispatch(actions.changeRoute(route))
      }
    }
    
    export default connect(
      mapStateToProps,
      mapDispatchToProps
    )(CelesteHiring)
  