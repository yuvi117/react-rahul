import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Field } from "react-final-form";
import createDecorator from 'final-form-focus'

import * as actions from "../../../../store/actions/index";
import ContactForm from "../../../../components/Forms/ContactForm/ContactForm";
import ProfileForm from "../../../../components/Forms/ProfileForm/ProfileForm";
import AvalabilityForm from "../../../../components/Forms/AvailabilityForm/AvailabilityForm";
import PersonalityForm from "../../../../components/Forms/PersonalityForm/PersonalityForm";
import TestForm from "../../../../components/Forms/TestForm/TestForm";
import Aux from "../../../../hoc/Auxx/Auxx";
import { routes } from "../../../../shared/constants";

class CelesteForm extends Component {
  state = {
    focusOnError: createDecorator(),
    formData: {
      contact: {
        name: "",
        dob: "",
        lastSchool: "",
        phone: "",
        countryCode: "",
        skills: "",
        experience: "",
        social: "",
        about: ""
      },
      profile: {
        services: "a",
        vehicle: "a",
        licence: "",
        hourlyRate: "",
        pic: ""
      },
      availability: {
        available: { fullTime: 0, morning: 0, evening: 0, night: 0 },
        blockDays: { mon: 0, tue: 0, yed: 0, thu: 0, fri: 0, sat: 0 },
        calender: { available: 0, blocked: 0 }
      },
      personality: {
        "1": "",
        "2": "",
        "3": "",
        "4": "",
        "5": "",
        "6": "",
        "7": "",
        "8": ""
      },
      test: {
        "1": "",
        "2": "",
        "3": ""
      }
    }
  };

  componentDidMount() {
    this.props.onComponentsVisibilityChange({ footer: false });
    this.props.onRouteChange(routes.CELESTE_HIRING);

  }

  componentWillUnmount() {
    this.props.onComponentsVisibilityChange({ footer: true });
  }

  imageSelectedHandler = (e, pathArray) => {
    let updatedFormData = {
      ...this.state.formData
    };
    updatedFormData[pathArray[0]][pathArray[1]] =
      e.target.files && e.target.files.length > 0
        ? e.target.files
        : this.state.formData[pathArray[0]][pathArray[1]];
    this.setState({ formData: updatedFormData });
  };

  inputChangeHandler = (e, pathArray) => {
    let updatedFormData = {
      ...this.state.formData
    };
    let tempObj = updatedFormData;
    for (let i = 0; i < pathArray.length - 1; i++) {
      tempObj = tempObj[pathArray[i]];
    }
    tempObj[pathArray[pathArray.length - 1]] = e.target.value;

    this.setState({ formData: updatedFormData });
  };

  onSubmitHandler = (values) => {
    console.log('val',values)
    const formData = {
      contact: {
        name: values.Name,
        dob: values['Date of Birth'],
        lastSchool: values['Last School'],
        phone: values['Phone Number'],
        countryCode: values.Name,
        skills: values.Skills,
        experience: values.Experience,
        social: values.Name,
        about: values.About,
      },
      profile: {
        services: values.Service,
        vehicle: values.Vehicle,
        licence: values['Work Licence'],
        hourlyRate: values['Hourly rate'],
        pic: ""
      },
      availability: {
        available: { fullTime: 0, morning: 0, evening: 0, night: 0 },
        blockDays: { mon: 0, tue: 0, yed: 0, thu: 0, fri: 0, sat: 0 },
        calender: { available: 0, blocked: 0 }
      },
      personality: {
        "1": "",
        "2": "",
        "3": "",
        "4": "",
        "5": "",
        "6": "",
        "7": "",
        "8": ""
      },
      test: {
        "1": "",
        "2": "",
        "3": ""
      }
    }
  };

  render() {
    // const focusOnError = createDecorator()
    return (
      <Aux>
        <Form
          onSubmit={this.onSubmitHandler}
          decorators={[this.state.focusOnError]}
          subscription={{ submitting: true }}
        >
          {({ handleSubmit, values, submitting }) => (
            <form onSubmit={handleSubmit}>
              <ContactForm
                formData={this.state.formData}
                changeInput={this.inputChangeHandler}
                values={values}
                Field={Field}
              />
              <ProfileForm
                formData={this.state.formData}
                changeInput={this.inputChangeHandler}
                imageUpload={this.imageSelectedHandler}
                Field={Field}
              />
              <AvalabilityForm
                formData={this.state.formData}
                changeInput={this.inputChangeHandler}
                Field={Field}
              />
              <PersonalityForm
                formData={this.state.formData}
                changeInput={this.inputChangeHandler}
                Field={Field}
              />
              <TestForm
                formData={this.state.formData}
                changeInput={this.inputChangeHandler}
                Field={Field}
              />
            </form>
          )}
        </Form>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath,
    surveys: state.auth.surveys
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onRouteChange: route => dispatch(actions.changeRoute(route)),
    onComponentsVisibilityChange: status =>
      dispatch(actions.changeVisibility(status))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CelesteForm);
