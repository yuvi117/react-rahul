import React, { Component } from "react";
import { connect } from "react-redux";
import Spinner from "../../../../components/UI/Spinner/Spinner1";

// eslint-disable-next-line
import "!style-loader!css-loader!./Network.css";
import * as actions from "../../../../store/actions/index";

class Network extends Component {
  componentDidMount() {
    this.props.onNetworkFetch(this.props.token);
  }

  render() {
    const networkCopy = [...this.props.network]
    const networks = [networkCopy.splice(0, 6)];
    if (networkCopy && networkCopy > 6) {
      networks.push(networkCopy)
    }
    return (
      <section
        className="network_ylwlit space_100"
        style={{ fontSize: "x-large" }}
      >
        <div className="container">
          <h4>Network</h4>
          {this.props.loading1 ? <Spinner /> : this.props.network.length < 1 ? (<p style={{textAlign: "-webkit-center"}}>No Network</p>) : networks.map((network, index) => <div
            key={index}
            className="row_cus"
            style={{
              display: "-webkit-flex"
            }}
          >
            {network.map(user => (
              <div key={user._id} className="col-md-2 col-sm-6">
                <div className="round_lit">
                  <img
                    src={
                      user.userPics && user.userPics.length > 0 && user.userPics.join()
                        ? `http://139.59.18.239:6017/image/${user.userPics[0]}`
                        : require("./../../../../assests/Images/Portrait_Placeholder.png")
                    }
                    alt="pic"
                  />
                </div>
              </div>
            ))}
          </div>)
            }
        </div>
        <div className="find_more text-center">
          <button type="submit" className="btn btn-default">
            Find More
          </button>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading1: state.network.loading1,
    error: state.auth.error,
    network: state.network.network,
    isAuthenticated: state.auth.token !== null,
    token: state.auth.token,
    authRedirectPath: state.auth.authRedirectPath
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onRouteChange: route => dispatch(actions.changeRoute(route)),
    onNetworkFetch: route => dispatch(actions.getMyNetwork(route))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Network);
