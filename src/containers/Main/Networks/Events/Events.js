import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, NavLink } from 'react-router-dom'

// eslint-disable-next-line
import '!style-loader!css-loader!./Events.css'
import * as actions from '../../../../store/actions/index'

class Events extends Component {

  render () {
    return (
        <section className="network_ylwlit events" style={{fontSize: "x-large"}}>
			<div className="container">
				<h4>{this.props.name}</h4>
				<div className="events_div down_space">
					<div 
					className="row_cus"
					 style={{
						display: "-webkit-flex",
					  }}>
						<div className="col-md-2 col-sm-2">
							<div className="normal_lit">
								<img src={require('./../../../../assests/Images/home_bg.jpg')}/>
							</div>
						</div>

						<div className="col-md-2 col-sm-2">
							<div className="normal_lit">
								<img src={require('./../../../../assests/Images/booktour_bg.jpg')}/>
							</div>
						</div>

						<div className="col-md-2 col-sm-2">
							<div className="normal_lit">
								<img src={require('./../../../../assests/Images/cal.jpg')}/>
							</div>
						</div>

						<div className="col-md-2 col-sm-2">
							<div className="normal_lit">
								<img src={require('./../../../../assests/Images/1385.jpg')}/>
							</div>
						</div>

						<div className="col-md-2 col-sm-2">
							<div className="normal_lit">
								<img src={require('./../../../../assests/Images/painting_hd.jpg')}/>
							</div>
						</div>

						<div className="col-md-2 col-sm-2">
							<div className="normal_lit">
								<img src={require('./../../../../assests/Images/sunset.jpg')}/>
							</div>
						</div>
					</div>
					<div className="find_more text-center">
					<button type="submit" className="btn btn-default">Find More</button>
				</div>
				</div>
				
			</div>
		</section>
      )
    }
  }
  
  const mapStateToProps = state => {
      return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        surveys: state.auth.surveys
      }
    }
    
    const mapDispatchToProps = dispatch => {
      return {
        onAuth: (email, password) => dispatch(actions.auth(email, password)),
        onRouteChange: route => dispatch(actions.changeRoute(route))
      }
    }
    
    export default connect(
      mapStateToProps,
      mapDispatchToProps
    )(Events)
  