import React, { Component } from 'react'
import { connect } from 'react-redux'

// eslint-disable-next-line
import '!style-loader!css-loader!./Networks.css'
import * as actions from '../../../store/actions/index'
import { routes } from '../../../shared/constants';
import Network from './Network/Network'
import Events from './Events/Events'
import Aux from '../../../hoc/Auxx/Auxx'

class CelesteHiring extends Component {

  componentDidMount () {
    this.props.onRouteChange(routes.NETWORKS);
    if (this.props.notifications.network) {
      this.props.onChangeNotification({network: 0})
    }
  }
  render () {
    return (
        <Aux>
    <Network/>
    <Events name={'Events'}/>
    <Events name={'Files'}/>
    </Aux>
      )
    }
  }
  
  const mapStateToProps = state => {
      return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        notifications: state.auth.notifications,
        surveys: state.auth.surveys
      }
    }
    
    const mapDispatchToProps = dispatch => {
      return {
        onAuth: (email, password) => dispatch(actions.auth(email, password)),
        onRouteChange: route => dispatch(actions.changeRoute(route)),
        onChangeNotification: status => dispatch(actions.changeNotification(status))

      }
    }
    
    export default connect(
      mapStateToProps,
      mapDispatchToProps
    )(CelesteHiring)
  