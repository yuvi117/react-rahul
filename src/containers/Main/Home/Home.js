import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";

import classes from "./Home.css";
import * as actions from "../../../store/actions/index";
import Aux from "../../../hoc/Auxx/Auxx";
import { routes } from "../../../shared/constants";
import Profile from "../../../components/profile/profile";
import withErrorHandler from "../../../hoc/withErrorHandler/withErrorHandler";
import { axiosInstance as axios } from "../../../axios-orders";

class Home extends Component {
  state = {
    showProfile: false,
    onlineStatus: false,
    intervalID: null,
  };

  componentDidMount() {
    this.props.onRouteChange(routes.HOME);
    this.props.onUsersFetch(this.props.token);
    window.scrollTo(0, 0)

  }

  showProfleHandler = () => {
    this.setState(prevState => {
      return { showProfile: !prevState.showProfile };
    });
  };

  checkOnlineStatus = user_id => {
    this.props.socket.emit("checkStatus", user_id, status => {
      if (status) {
        console.log('if')
        this.setState((prevState) => {
          return { onlineStatus: true};
        });
      } else {
        console.log('else')
        // this.setState({ onlineStatus: false });
      }
    });
  };

  takeAction = (user_id, actionType) => {
    const actionData = {
      action: actionType,
      actionOnUser: user_id
    };
    this.props.onAction(this.props.token, actionData, this.props.onUsersFetch);
  };

  render() {
    const setOnlineStatusInterval = user_id => {
      this.checkOnlineStatus(user_id);
      console.log("setOnlinefunc");
      this.setState({ intervalID: setInterval(() => this.checkOnlineStatus(user_id), 2000) });
    };

    const clearOnlineStatusInterval = () => {
      console.log('clear')
      clearInterval(this.state.intervalID);
      this.setState({ onlineStatus: false });
    };
    return (
      <Aux>
        <section className={`${classes.looking_for} ${classes.space_100}`}>
          <div className="container">
            <h1 className="text-center">What are you looking for ?</h1>
            <div 
            className={classes.row_cus}
             style={{
              display: "-webkit-flex",
            }}>
              {[
                { name: "Travel", bg: false, link: "/BookTour" },
                { name: "Network", bg: true, link: "/Networks" },
                { name: "Hiring", link: "/celesteHiring", bg: false },
                { name: "Contact us", link: "", bg: true }
              ].map(data => {
                return (
                  <div key={data.name} className="col-md-3 col-sm-6">
                    <div className={classes.padding_looking_div}>
                      <div
                        className={`${classes.adv_6} ${
                          data.bg ? classes.bg_adv : null
                        } text-center`}
                      >
                        <NavLink to={data.link}>{data.name}</NavLink>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </section>

        <section className={classes.experience_video}>
          <div className={classes.animate_btn}>
            <a className={classes.play_btn} href="#" />
          </div>
          <video width="700px" controls>
            <source
              src="http://clips.vorwaerts-gmbh.de/VfE_html5.mp4"
              type="video/mp4"
            />
          </video>
        </section>

        <section className={classes.round_list_sec}>
          <div className="container">
            <div className={`${classes.inner_list_round}  text-center`}>
              <img
                className={classes.round_img}
                src={require("./../../../assests/Images/round_bg.png")}
                alt='pic'
              />

              <div className={classes.friends_list}>
                {this.props.users.length > 0 ? (
                  this.props.users.map((item, index) => (
                    <div
                      key={item._id}
                      className={`${classes[`f${index + 1}`]} ${
                        classes.dropdown
                      } pro_card_`}
                      onMouseEnter={() => setOnlineStatusInterval(item._id)}
                      onMouseLeave={clearOnlineStatusInterval}
                    >
                      <button className={classes.dropbtn}>
                      
                        <img
                          src={item.userPics && item.userPics.length > 0 ? `http://139.59.18.239:6017/image/${
                            item.userPics[0]}` : require("./../../../assests/Images/placeholder.png")}
                            alt='pic'
                        />
                      </button>
                      <Profile
                        data={item}
                        onlineStatus={this.state.onlineStatus}
                        takeAction={this.takeAction}
                      />
                    </div>
                  ))
                ) : (
                  <div style={{ marginTop: "261px" }}>
                    <strong>No more Users</strong>
                  </div>
                )}
              </div>

              <div className={classes.find_more_btn}>
                <button
                  type="submit"
                  className={`${classes.btn} btn-default`}
                  data-toggle="modal"
                  data-target="#myModal"
                >
                  Find more
                </button>
              </div>

              <div className="modal fade" id="myModal">
                <div className="modal-dialog">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h4 className="modal-title">Modal Heading</h4>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                      >
                        &times;
                      </button>
                    </div>

                    <div className="modal-body">Modal body..</div>

                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-danger"
                        data-dismiss="modal"
                      >
                        Close
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    users: state.users.similarIntersetUsers,
    token: state.auth.token,
    socket: state.auth.socket
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUsersFetch: token => dispatch(actions.getusersWithSimilarInterest(token)),
    onAction: (token, data) => dispatch(actions.takeAction(token, data)),
    onRouteChange: route => dispatch(actions.changeRoute(route))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(Home, axios));
