import React, { Component } from 'react'
import { connect } from 'react-redux'

import classes from './BookTour.css'
import * as actions from '../../../store/actions/index'
import Aux from '../../../hoc/Auxx/Auxx'
import { routes } from '../../../shared/constants';

class BookTour extends Component {
  componentDidMount () {
    this.props.onRouteChange(routes.BOOK_TOUR);
  }

  render () {
    return (
      <Aux>
        <section className={`${classes.looking_for} ${classes.space_100}`}>
          <div className='container'>
            <h1 className='text-center'>What are you looking for ?</h1>
            <div 
            className={classes.row_cus}
             style={{
              display: "-webkit-flex",
            }}>
              <div className='col-md-3 col-sm-6'>
                <div className={classes.img_tour}>
                  <img src={require('./../../../assests/Images/Celestebg.jpg')} />
                  <div className={classes.rating_stars}>
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                  </div>

                  <div className={`${classes.like_tour} text-center`}>
                    <a href='#' className={classes.left}>
                      <i className='fa fa-check' />
                    </a>
                    <a href='#'>
                      <i className='fa fa-heart' />
                    </a>
                    <a href='#' className={classes.right}>
                      <i className='fa fa-times' />
                    </a>
                  </div>

                  <div className={classes.place_name}>
                    <p>Moscow</p>
                    <p className={classes.days}>30days</p>
                  </div>
                </div>
              </div>

              <div className='col-md-3 col-sm-6'>
                <div className={classes.img_tour}>
                  <img src={require('./../../../assests/Images/sunset.jpg')} />
                  <div className={classes.rating_stars}>
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                  </div>

                  <div className={`${classes.like_tour} text-center`}>
                    <a href='#' className={classes.left}>
                      <i className='fa fa-check' />
                    </a>
                    <a href='#'>
                      <i className='fa fa-heart' />
                    </a>
                    <a href='#' className={classes.right}>
                      <i className='fa fa-times' />
                    </a>
                  </div>

                  <div className={classes.place_name}>
                    <p>Moscow</p>
                    <p className={classes.days}>30days</p>
                  </div>
                </div>
              </div>

              <div className='col-md-3 col-sm-6'>
                <div className={classes.img_tour}>
                  <img src={require('./../../../assests/Images/Celestebg.jpg')} />
                  <div className={classes.rating_stars}>
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                  </div>

                  <div className={`${classes.like_tour} text-center`}>
                    <a href='#' className={classes.left}>
                      <i className='fa fa-check' />
                    </a>
                    <a href='#'>
                      <i className='fa fa-heart' />
                    </a>
                    <a href='#' className={classes.right}>
                      <i className='fa fa-times' />
                    </a>
                  </div>

                  <div className={classes.place_name}>
                    <p>Moscow</p>
                    <p className='days'>30days</p>
                  </div>
                </div>
              </div>

              <div className='col-md-3 col-sm-6'>
                <div className={classes.img_tour}>
                  <img src={require('./../../../assests/Images/sunset.jpg')} />
                  <div className={classes.rating_stars}>
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                    <i className='fa fa-star' />
                  </div>

                  <div className={`${classes.like_tour} text-center`}>
                    <a href='#' className={classes.left}>
                      <i className='fa fa-check' />
                    </a>
                    <a href='#'>
                      <i className='fa fa-heart' />
                    </a>
                    <a href='#' className={classes.right}>
                      <i className='fa fa-times' />
                    </a>
                  </div>

                  <div className={classes.place_name}>
                    <p>Moscow</p>
                    <p className={classes.days}>30days</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className={classes.experience_video}>
          <div className={classes.animate_btn}>
            <a className={classes.play_btn} href='#' />
          </div>
          <video width='700px' controls>
            <source
              src='http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'
              type='video/mp4'
            />
          </video>
        </section>
      </Aux>
    )
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath,
    surveys: state.auth.surveys
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password) => dispatch(actions.auth(email, password)),
    onRouteChange: route => dispatch(actions.changeRoute(route))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookTour)
