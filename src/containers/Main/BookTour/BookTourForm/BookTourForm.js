import React, { Component } from 'react'
import { connect } from 'react-redux'

import Input from '../../../../components/UI/Input/Login/InputLogin'
import Spinner from '../../../../components/UI/Spinner/Spinner'
import classes from './BookTourForm.css'
import * as actions from '../../../../store/actions/index'
import { updateObject, checkValidity } from '../../../../shared/utility'
import Aux from '../../../../hoc/Auxx/Auxx'

class BookTourForm extends Component {
  state = {
    controls: {
      email: {
        elementType: 'input',
        message: 'This is required',
        class: 'rs1WrapInput100',
        Iconclass: 'fa fa-envelope',
        elementConfig: {
          type: 'email',
          placeholder: 'Enter username / Email'
        },
        value: '',
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: 'input',
        message: 'This is required',
        class: 'rs2WrapInput100',
        Iconclass: 'fa fa-lock',
        elementConfig: {
          type: 'password',
          placeholder: 'Enter Password'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      name: {
        elementType: 'input',
        message: 'This is required',
        class: 'rs2WrapInput100',
        Iconclass: 'fa fa-lock',
        elementConfig: {
          type: 'text',
          placeholder: 'Enter Name'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      number: {
        elementType: 'input',
        message: 'This is required',
        class: 'rs2WrapInput100',
        Iconclass: 'fa fa-lock',
        elementConfig: {
          type: 'text',
          placeholder: 'Enter Name'
        },
        value: '',
        validation: {
          required: true,
          isNumeric: true,
          minLength: 10
        },
        valid: false,
        touched: false
      },
      location: {
        elementType: 'input',
        message: 'This is required',
        class: 'rs2WrapInput100',
        Iconclass: 'fa fa-lock',
        elementConfig: {
          type: 'text',
          placeholder: 'Enter Name'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      repeatPassword: {
        elementType: 'input',
        message: 'This is required',
        class: 'rs2WrapInput100',
        Iconclass: 'fa fa-lock',
        elementConfig: {
          type: 'text',
          placeholder: 'Enter Name'
        },
        value: '',
        validation: {
          required: true,
          compare: true
        },
        valid: false,
        touched: false
      }
    },
    formIsValid: false,
    validationMessage: '',
    submit: false,
    currentSurvey: 1,
    address: ''
  }

  changeSurveyHandler = () => {
    if (this.state.currentSurvey < Object.keys(this.props.surveys).length) {
      this.setState(prevState => {
        return {
          currentSurvey: prevState.currentSurvey + 1
        }
      })
    } else {
      // History.goBack()
      // History.push('/Login')
    }
  }

  inputChangedHandler = (event, controlName, extraValues) => {
    const validationData = checkValidity(
      event.target.value,
      this.state.controls[controlName].validation,
      controlName,
      { ...extraValues }
    )
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event.target.value,
        valid: validationData.isValid,
        touched: true,
        message: validationData.message
      })
    })
    if (
      controlName === 'password' &&
      this.state.controls.repeatPassword.valid === true &&
      this.state.controls.repeatPassword.value !== event.target.value
    ) {
      updatedControls['repeatPassword'].valid = false
    }
    let formIsValid = true
    for (let inputIdentifier in updatedControls) {
      formIsValid = updatedControls[inputIdentifier].valid && formIsValid
    }
    this.setState({
      controls: updatedControls,
      formIsValid: formIsValid,
      validationMessage: validationData.isValid ? null : validationData.message
    })
  }

  submitHandler = event => {
    event.preventDefault()
    if (!this.state.formIsValid) {
      const updatedControls = {
        ...this.state.controls
      }
      for (let inputIdentifier in updatedControls) {
        updatedControls[inputIdentifier].touched = true
      }
      this.setState({
        controls: updatedControls
      })
    } else {
      this.setState({
        submit: true
      })
    }
  }

  render () {
    const formElementsArray = []
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key]
      })
    }

    let form = formElementsArray.map(formElement => (
      <Input
        key={formElement.id}
        classN={classes.input100}
        IconClass={formElement.config.Iconclass}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        divClass={formElement.config.class}
        message={formElement.config.message}
        changed={event => this.inputChangedHandler(event, formElement.id)}
      />
    ))

    if (this.props.loading) {
      form = <Spinner />
    }

    let errorMessage = null

    if (this.props.error) {
      errorMessage = <p>{this.props.error.message}</p>
    }

    let authRedirect = null
    if (this.props.isAuthenticated) {
    }

    return (
      <Aux>
      <div className="container">
      <div className={classes.form_booktour}>
        <h2>Book a Business Tour</h2>
        {authRedirect}
        <form onSubmit={this.submitHandler}>
            <div className={`${classes.form_group} ${classes.select_box}`}>
              <i className="fa fa-chevron-down"></i>
              <select className={classes.form_control}>
                <option value="" disabled defaultValue>Industry</option>
                <option value="hurr">Saab</option>
                <option value="hurr">Opel</option>
                <option value="hurr">Audi</option>
              </select>
            </div>

            <div className={`${classes.form_group} ${classes.partition_inputs}`}>
              <input type="text" className={`${classes.form_control} ${classes.right_space}`} placeholder="Where"/>
              <input type="text" className={classes.form_control} placeholder="Number of guests"/>
            </div>

            <div className={`${classes.form_group} ${classes.partition_inputs}`}>
              <input type="text" className={`${classes.form_control} ${classes.right_space}`} placeholder="From"/>
              <input type="text" className={classes.form_control} placeholder="To"/>
            </div>
        {errorMessage}
            <div className={`${classes.btn_work} text-center`}>
              <button style={{color:"#000"}} type='submit' className={`${classes.btn} btn-default submit`}>Submit</button>
            </div>
        </form>
      </div>
    </div>
      </Aux>
    )
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath,
    surveys: state.auth.surveys
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password) => dispatch(actions.auth(email, password))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookTourForm)
