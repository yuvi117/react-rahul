export const port = {
    prodction: 7000,
    development: 5000,
} 

export const routes = {
    LOGIN: 1,
    SIGNUP: 2,
    HOME: 3,
    BOOK_TOUR: 4,
    CELESTE_HIRING: 5,
    NETWORKS: 6,
    LOGOUT: 10
}