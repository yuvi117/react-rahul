// const crypto = require('crypto');
import crypto from "crypto";
const algorithm = "aes-256-cbc";


const key = "d6F3Efeqd6F3Efeqd6F3Efeqd6F3Efeq";
const iv = "e4rt6y7ui89oer5t";
// KEY = Buffer.from('some key string', 'hex');

export const encrypt = text => {
  let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return { iv: iv.toString("hex"), encryptedData: encrypted.toString("hex") };
};

export const decrypt = text => {
  let iv = text.iv;
  let encryptedText = Buffer.from(text.encryptedData, "hex");
  let decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
};

export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  };
};

export const checkValidity = (value, rules, controlName, extraValues) => {
  let isValid = true;
  let message = "";
  if (!rules) {
    return true;
  }

  if (rules.required && isValid) {
    isValid = value.trim() !== "" && isValid;
    message = `This field is Required`;
  }

  if (rules.isNumeric && isValid) {
    const pattern = /^\d+$/;
    isValid = pattern.test(value) && isValid;
    message = `Should be a number`;
  }

  if (rules.minLength && isValid) {
    isValid = value.length >= rules.minLength && isValid;
    message = `Should be atleast ${rules.minLength} digits`;
  }

  if (rules.maxLength && isValid) {
    isValid = value.length <= rules.maxLength && isValid;
    message = `${controlName} should be less than ${rules.maxLength}`;
  }

  if (rules.isEmail && isValid) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
    message = `Enter a valid email`;
  }

  if (rules.compare && isValid) {
    isValid = value === extraValues.password && isValid;
    message = `Password is incorrect`;
  }

  return { isValid, message };
};

export const getGMTTime = (after) => {
  let daysInMs = 1000*60*60*24*after||0
 return new Date(Date.now() + (daysInMs))
}
