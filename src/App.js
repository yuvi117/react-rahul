import React, { Component } from "react";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import * as actions from "./store/actions/index";
import asyncComponent from "./hoc/asyncComponent/asyncComponent";
import "./App.css";
import AuthLayout from "./hoc/AuthLayout/AuthLayout";
import celesteHiringForm from "./containers/Main/CelesteHiring/CelesteForm/CelesteForm";
import login from "./containers/Auth/Login/Login";
import signUp from "./containers/Auth/SignUp/SignUp";
import withErrorHandler from "./hoc/withErrorHandler/withErrorHandler";
import socket from "./hoc/sockets/sockets";
import { axiosInstance as axios } from "./axios-orders";

const asyncForgotPassword = asyncComponent(() => {
  return import("./containers/Auth/ForgotPassword/ForgotPassword");
});

const asyncHome = asyncComponent(() => {
  return import("./containers/Main/Home/Home");
});

const asyncBookTour = asyncComponent(() => {
  return import("./containers/Main/BookTour/BookTour");
});

const asyncCelesteHiring = asyncComponent(() => {
  return import("./containers/Main/CelesteHiring/CelesteHiring");
});

const asyncNetworks = asyncComponent(() => {
  return import("./containers/Main/Networks/Networks");
});

class App extends Component {
  componentDidMount() {
    this.props.onTryAutoSignup();
  }

  componentDidUpdate() {
    if (
      this.props.isAuthenticated &&
      (!this.props.socket || !this.props.socket.connected)
    ) {
      if (this.props.socket) {
        this.props.socket.connect();
      }
    }
    console.log("url", window.location.href);
  }

  render() {
    let routes = null;

    if (this.props.isAuthenticated) {
      routes = (
        <AuthLayout>
          <Switch>
            <Route path="/BookTour" exact component={asyncBookTour} />
            <Route path="/Home" exact component={asyncHome} />
            <Route path="/celesteHiring" exact component={asyncCelesteHiring} />
            <Route
              path="/celesteHiring/celeste_form"
              exact
              component={celesteHiringForm}
            />
            <Route path="/Networks" exact component={asyncNetworks} />

            <Route path="/Login" exact component={login} />

            <Redirect to="/Home" />
          </Switch>
        </AuthLayout>
      );
    }

    if (this.props.notAuthenticated === true) {
      routes = (
        <AuthLayout>
          <Switch>
            <Route path="/Login" exact component={login} />
            <Route path="/SignUp" component={signUp} />
            <Route path="/forgetPassword" component={asyncForgotPassword} />

            <Redirect to="/Login" />
          </Switch>
        </AuthLayout>
      );
    }
    return <div>{routes}</div>;
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token,
    notAuthenticated: state.auth.token === null,
    token: state.auth.token,
    currentRoute: state.auth.currentRoute,
    userId: state.auth.userId,
    socket: state.auth.socket
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
    onChangeNotification: status => dispatch(actions.changeNotification(status))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withErrorHandler(socket(App), axios))
);
