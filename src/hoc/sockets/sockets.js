import React, { Component } from "react";
import { connect } from "react-redux";
import io from "socket.io-client";

import Aux from "../Auxx/Auxx";
import * as actions from "./../../store/actions/index";

const socket = io("http://139.59.18.239:6017");

const sockets = WrappedComponent => {
  class soc extends Component {
    state = {
      userAdded: false
    };
    constructor() {
      super();
      socket.on("connect", () => {
        console.log("connected");
      });

      socket.on("userStatus", ack => {
        ack(true);
      });

      socket.on("disconnect", err => {
        console.log("seerrr", err);
      });

      socket.on("notification", notification => {
        this.props.onChangeNotification(notification);
      });
    }

    componentDidMount() {
      
    }

    componentDidUpdate() {
        if (!this.props.socket) {
            this.props.onUserConnected(socket);
          }
      if (
        this.props.isAuthenticated &&
        (!this.props.socket || !this.props.socket.connected)
      ) {
        if (this.props.socket) {
          this.props.socket.connect();
        } else {
        }
      }

      if (
        this.props.isAuthenticated &&
        this.props.socket &&
        !this.state.userAdded
      ) {
        console.log("added user");
        this.props.socket.emit("addUser", this.props.userId);
        this.setState({ userAdded: true });
      }

      if (this.state.userAdded && !this.props.userId) {
        this.setState({ userAdded: false });
      }
    }

    render() {
      return (
        <Aux>
          <WrappedComponent {...this.props} />
        </Aux>
      );
    }
  }
  const mapStateToProps = state => {
    return {
      isAuthenticated: state.auth.token,
      token: state.auth.token,
      socket: state.auth.socket
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      onUserConnected: socket => dispatch(actions.socketConnected(socket)),
      onChangeNotification: status =>
        dispatch(actions.changeNotification(status))
    };
  };

  soc.displayName = `WithSubscription(${getDisplayName(WrappedComponent)})`;
  return connect(mapStateToProps, mapDispatchToProps)(soc);
};

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || "Component";
}

export default sockets;
