import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as actions from './../../store/actions/index'
import NavigationItems from '../../components/Navigation/NavigationItems/NavigationItems'
import Footer from '../../components/Footer/Footer'
import { routes } from '../../shared/constants'
import History from '../../store/History/History'
import Logout from '../../containers/Auth/Logout/Logout'

class AuthLayout extends Component {
  state = {
    firstRenderHistoryLength: null,
    currentHistoryLength: null,
    historyLength: window.history.length,
    showPopup: false
  }
  componentDidMount () {
    if (this.state.firstRenderHistoryLength === null) {
      this.setState({
        firstRenderHistoryLength: window.history.length
      })
    }
  }

  navItemClickedHandler = (itemIndex, route) => {
     if (route) {
      this.props.onRouteChange(route)
    } else {
      this.setState((prevState) => { return { showPopup: !prevState.showPopup }})
    }
  }

  closePopupHandler = () => {
    this.setState({ showPopup: false })
  }

  backButtonHandler = () => {
    console.log('firstRenderHistoryLength', this.state.firstRenderHistoryLength)
    console.log('currentHistoryLength', this.state.currentHistoryLength)
    console.log('window.history.length', window.history.length)
    let h = window.history.length
    if (h !== this.state.firstRenderHistoryLength) {
      History.goBack()
    }
    console.log('after if window.history.length', window.history.length)
  }
  render () {
    let image = ''
    let classname = ''

    if (routes.LOGIN === this.props.route) {
      image = 'sunset.jpg'
      classname = 'vedi_login'
    } else if (routes.HOME === this.props.route) {
      image = 'home_bg.jpg'
      classname = 'homepage_vedi'
    } else if (routes.BOOK_TOUR === this.props.route) {
      image = 'booktour_bg.jpg'
      classname = 'booktour_page'
    } else if (routes.CELESTE_HIRING === this.props.route) {
      image = 'cal.jpg'
      classname = 'celeste_hiringpage'
    } else if (routes.NETWORKS === this.props.route) {
      image = 'mountains.png'
      classname = 'celeste_hiringpage'
    }
    return (
          <div className={`cel_formpage ${classname}`}>
        <NavigationItems
          classN={classname}
          image={image}
          notifications={this.props.notifications}
          authenticated={this.props.isAuthenticated}
          onClick={this.navItemClickedHandler}
          currentNav={this.props.route}
          showTourForm={routes.BOOK_TOUR === this.props.route}
          backButton={this.backButtonHandler}
        />
        {console.log('render window.history.length', window.history.length)}
        <Logout showPopup={this.state.showPopup} logout={this.navItemClickedHandler} closePopup={this.closePopupHandler} />
        {this.props.children}
        {this.props.visibility.footer ? <Footer /> : null}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    route: state.auth.currentRoute,
    notifications: state.auth.notifications,
    visibility: state.auth.componentsVisibility
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onRouteChange: route => dispatch(actions.changeRoute(route)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthLayout)
