import React, { Component } from "react";
import { connect } from "react-redux";

import Modal from "../../components/UI/Modal/Modal";
import Aux from "../Auxx/Auxx";
import * as actions from "./../../store/actions/index";

const withErrorHandler = (WrappedComponent, axios) => {
  class Compo extends Component {
    state = {
      error: null
    };
    constructor() {
      super();
      this.reqInterceptor = axios.interceptors.request.use(req => {
        this.setState({ error: null });
        return req;
      });
      this.resInterceptor = axios.interceptors.response.use(
        res => res,
        error => {
          console.log("errapi", error.message);
          // console.log("reserror", error.response.data.status);
          this.setState({ error: error });
        }
      );
    }

    componentWillUnmount() {
      axios.interceptors.request.eject(this.reqInterceptor);
      axios.interceptors.response.eject(this.resInterceptor);
    }

    errorConfirmedHandler = () => {
      this.setState({ error: null });
    };

    render() {
      let message = "";
      let errorFunc = this.errorConfirmedHandler;
      if (
        this.state.error &&
        this.state.error.response &&
        this.state.error.request
      ) {
        if (
          this.state.error.response.data.status === 400 &&
          this.state.error.request.status === 401
        ) {
          message = "Session Expired, Please Login Again.";
          errorFunc = () => this.props.onLogout(this.props.token);
        } else {
          message = this.state.error.response.data.message;
        }
      } else {
        message = "No Internet Connection";
      }
      return (
        <Aux>
          <Modal show={this.state.error}>
            {this.state.error ? (
              <Aux>
                <div className="modal-header">
                  <h4
                    className="modal-title"
                    style={{ color: "brown", fontWeight: "bold" }}
                  >
                    Somthing went wrong
                  </h4>
                </div>
                <div className="modal-body" style={{ color: "darkgoldenrod" }}>
                  {message}
                </div>

                <div className="modal-footer">
                  <button
                    id="btnExpiredOk"
                    type="button"
                    onClick={errorFunc}
                    className="btn-primary"
                    data-dismiss="modal"
                    style={{
                      padding: "6px 12px",
                      marginBottom: "0",
                      fontSize: "14px",
                      fontWeight: "normal",
                      border: "1px solid transparent",
                      borderRadius: "4px",
                      backgroundColor: "orange",
                      color: "#FFF",
                      lineHeight: "1.5"
                    }}
                  >
                    Ok
                  </button>
                </div>
              </Aux>
            ) : null}
          </Modal>
          <WrappedComponent {...this.props} />
        </Aux>
      );
    }
  }
  const mapStateToProps = state => {
    return {
      isAuthenticated: state.auth.token !== null,
      token: state.auth.token
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      onLogout: token => dispatch(actions.logout(token))
    };
  };

  Compo.displayName = `WithSubscription(${getDisplayName(WrappedComponent)})`;
  return connect(mapStateToProps, mapDispatchToProps)(Compo);
};

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || "Component";
}

export default withErrorHandler;
