import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
// eslint-disable-next-line
import "!style-loader!css-loader!font-awesome/css/font-awesome.min.css";

import './index.css';
import App from './App';
import authReducer from './store/reducers/auth';
import usersReducer from './store/reducers/users';
import networkReducer from './store/reducers/network';
import registerServiceWorker from './registerServiceWorker';
import History from './store/History/History';

const composeEnhancers =
  process.env.NODE_ENV === 'development'
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose

const rootReducer = combineReducers({
  auth: authReducer,
  users: usersReducer,
  network: networkReducer,
})

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))

const app = (
  <Provider store={store}>
    <BrowserRouter basename="Vedi-App/#" history={History}>
    <App />
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
