import React from 'react'
// import {  } from 'react-bootstrap';

import classes from './UserProfile.css'

const userProfile = props => {
  return (
    <div
      className={`col-md-3 text-center ${classes.User}`}
      onClick={props.clicked}
      style={{ backgroundColor: 'transparent' }}
    >
      <img style={{width: props.sideDrawer ? '74%' : '54%', transition: 'width cubic-bezier(0.075, 0.82, 0.165, 1)'}}
        src={`https://s3.us-east-2.amazonaws.com/swift-spar-local/test_example/${
          props.profile_pic
        }`}
        alt='Osi'
      />

      <h6>{props.user_name}</h6>
    </div>
  )
}

export default userProfile
