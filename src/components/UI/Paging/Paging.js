import React from 'react'

import classes from './Paging.css'


const paging = (props) => {
    let classname = [classes.pageLink]
    if(props.pageNo === props.currentPage) {
        classname = [classes.pageLink, classes.active]
    }
    return (
    <li className='page-item'>
              <i
                className={classname.join(' ')}
                onClick={props.changePage}
              >
                {props.pageNo}
              </i>
            </li>
)};

export default paging;