import React from 'react';

// eslint-disable-next-line
import "!style-loader!css-loader!./Spinner1.css";

const spinner1 = () => (
  <div className="loadingio-spinner-bars-quecsoezym9"><div className="ldio-37ujay7tybv">
  <div></div><div></div><div></div><div></div>
  </div></div>
);

export default spinner1;