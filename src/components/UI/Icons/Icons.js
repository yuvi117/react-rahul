import React from "react";

const icon = props => {
  let iconClass = "";
  let iconStyle = {
    color: "darkgoldenrod",
    fontSize: "16px",
    marginLeft: "245px"
  };
  switch (props.icon) {
    case "circle Spinner":
      iconClass = "fa fa-circle-o-notch fa-spin fa-3x fa-fw";
      break;
    case "check":
      iconClass = "fa fa-check";
      iconStyle.color = "green";
      break;
    case "times":
      iconClass = "fa fa-times";
      iconStyle.color = "red";
      break;
    default:
      iconClass = "fa fa-times";
      iconStyle.color = "red";
  }
  return (
    <i
      className={iconClass}
      aria-hidden="true"
      style={props.style || iconStyle}
    />
  );
};

export default icon;
