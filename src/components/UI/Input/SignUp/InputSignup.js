import React from "react";

import classes from "./../../../../containers/Auth/SignUp/SignUp.css";
import Icon from "../../Icons/Icons";

const signupInput = props => {
  const { fieldProperties } = props;
  let onChangeHandler = (event) => props.onValueChange(event, fieldProperties.field)

  switch (fieldProperties.field) {
    case "email":
      onChangeHandler = (event) =>
        props.onValueChange(event, fieldProperties.field, {
          checkAvalability: true,
          payload: {
            userEmail: event.target.value.toString(),
            status: "1"
          }
        });
      break;
    case "repeatPassword":
        onChangeHandler = (event) =>
        props.onValueChange(event, fieldProperties.field, {
          password: props.extraValues
        });
      break;
    case "times":
      break;
    default:
  }
  return (
    <div
      className={`${classes.formGroup} ${props.rightSpace ? classes.right_space : null} ${
        classes["m-b-20"]
      } ${classes.wrapInput100} ${
        !fieldProperties.valid &&
        fieldProperties.validation &&
        fieldProperties.touched
          ? classes.alertValidate
          : null
      }`}
      datavalidate={fieldProperties.message}
    >
      <input
        type={fieldProperties.type}
        className={classes.formControl}
        placeholder={fieldProperties.placeholder}
        value={fieldProperties.value}
        onChange={onChangeHandler}
      />
      <i className={fieldProperties.Iconclass} />
      {props.children}
    </div>
  );
};

export default signupInput;
