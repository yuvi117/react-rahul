import React from 'react'

import classes from './InputLogin.css'
import Aux from '../../../../hoc/Auxx/Auxx'

const input = props => {
  let inputElement = null
  let alertClass = null
  // const inputClasses = props.classN ? [props.classN, classes.formControl] : [classes.InputElement]

  if (props.invalid && props.shouldValidate && props.touched) {
    alertClass = classes.alertValidate
  }

  switch (props.elementType) {
    case 'input':
      inputElement = (
        <input
          className={classes.formControl}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      )
      break
    case 'textarea':
      inputElement = (
        <textarea
          className={classes.formControl}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      )
      break
    case 'select':
      inputElement = (
        <select
          className={classes.formControl}
          value={props.value}
          onChange={props.changed}
        >
          {props.elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      )
      break
    default:
      inputElement = (
        <input
          className={classes.formControl}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      )
  }

  return (
    <Aux>
      <div
        className={`${classes.formGroup} ${classes['m-b-20']} ${classes.wrapInput100} ${alertClass || null}`}
        datavalidate={props.message}
      >
        {inputElement}
        <i className={props.IconClass} />
      </div>
    </Aux>
  )
}

export default input
