import React from "react";

// eslint-disable-next-line
import "!style-loader!css-loader!./AddImageButton.css";

const addImageButton = props => {
    const number = props.imageIndex || 0
  return (
    <div className="col-md-4">
      <div className="partner_detail add_member">
        <input
          type="file"
          style={{ display: "none" }}
          onChange={e => props.selectedImage(e, number)}
          ref={fileInput => (this[`fileInput${number}`] = fileInput)}
        />
        <button
          type="button"
          className="bn btn-default"
          onClick={() => this[`fileInput${number}`].click()}
          style={{ width: "50%" }}
        >
          <img
            style={{ marginBottom: "0px" }}
            src={
              props.file && props.file.length > 0
                ? URL.createObjectURL(props.file[0])
                : require("./../../../assests/Images/add_ic.png")
            }
            alt="select image"
          />
        </button>
      </div>
    </div>
  );
};

export default addImageButton;
