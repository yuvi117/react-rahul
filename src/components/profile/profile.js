import React from 'react'
import classes from './../../containers/Main/Home/Home.css'


const profile = props => (
    <div className={classes.dropdown_content} style={{left: '-110px', top: 'auto'}}>
    <div className={`${classes.profile_friend_hover} text-center`}>
      <div className={classes.online_status}>{props.onlineStatus ? <p>Online</p> : <p style={{color: 'red'}}>Offline</p>}</div>
      <img src={props.data.userPics && props.data.userPics.length > 0 ? `http://139.59.18.239:6017/image/${
        props.data.userPics[0]}` : require('./../../assests/Images/placeholder.png')} alt='user'/>
      <h5>{props.data.userFullname || 'user'}</h5>
      <p>Editor</p>
      <div className={classes.rating_stars_grp}>
        <i className="fa fa-star"></i>
        <i className="fa fa-star"></i>
        <i className="fa fa-star"></i>
        <i className="fa fa-star"></i>
        <i className="fa fa-star"></i>
    </div>

    <div className={`${classes.like_tour} text-center`}>
      <span style={{cursor: 'pointer'}} className={classes.left} onClick={() => props.takeAction(props.data._id, '1')}><i className="fa fa-check"></i></span>
      {/*<a href="#"><i className="fa fa-heart"></i></a>*/}
      <span style={{cursor: 'pointer'}} className={classes.right} onClick={() => props.takeAction(props.data._id, '2')}><i className="fa fa-times"></i></span>
    </div>

    {/*<div className={classes.invite_btn}>
      <button type="submit" className={`${classes.btn} btn-default ${classes.invite_btn_btn}`}>Invite</button>
    </div>*/}

    </div>
</div>
)

export default profile;
