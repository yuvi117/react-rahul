import React from 'react';

import burgerLogo from '../../assets/Images/osi_logo.png';
import classes from './Logo.css';

const logo = (props) => (
        <img src={burgerLogo} alt="OSI" className={classes.Logo}/>
);

export default logo;