import React from "react";

// eslint-disable-next-line
import "!style-loader!css-loader!./../ContactForm/ContactForm.css";

const profileForm = props => {
  const { Field } = props;
  const required = value => (value ? undefined : "Required");
  return (
    <section className="profile_sec">
      <div className="container">
        <div className="bg_fff">
          <div className="text-center">
            <h2>Profile</h2>
          </div>
          <div 
          className="row_cus" 
          style={{
            display: "-webkit-flex",
          }}>
            <div className="col-md-6">
              <Field
                name="Service"
                type="text"
                placeholder="Service you are offering"
                validate={required}
                initialValue={"a"}
                subscription={{
                  value: true,
                  active: true,
                  error: true,
                  touched: true
                }}
              >
                {({ input, meta, placeholder }) => (
                  <div
                    className="form-group addon_input"
                    style={{ position: "relative" }}
                  >
                    <select
                      {...input}
                      className="form-control"
                      placeholder={placeholder}
                    >
                      {[
                        { id: 1, value: "a" },
                        { id: 2, value: "b" },
                        { id: 3, value: "c" },
                        { id: 4, value: "d" }
                      ].map(option => (
                        <option key={option.id} value={option.value}>
                          {option.value}
                        </option>
                      ))}
                    </select>
                    {meta.error && meta.touched && (
                      <span
                        style={{
                          color: "red",
                          textDecoration: "none",
                          display: "list-item",
                          top: "0px",
                          right: "50px",
                          position: "absolute"
                        }}
                      >
                        {meta.error}
                      </span>
                    )}
                  </div>
                )}
              </Field>
              <Field
                name="Vehicle"
                type="text"
                placeholder="Vehicle"
                validate={required}
                initialValue={"a"}
                subscription={{
                  value: true,
                  active: true,
                  error: true,
                  touched: true
                }}
              >
                {({ input, meta, placeholder }) => (
                  <div
                    className="form-group addon_input"
                    style={{ position: "relative" }}
                  >
                    <select
                      {...input}
                      className="form-control"
                      placeholder={placeholder}
                    >
                      {[
                        { id: 1, value: "a" },
                        { id: 2, value: "b" },
                        { id: 3, value: "c" },
                        { id: 4, value: "d" }
                      ].map(option => (
                        <option key={option.id} value={option.value}>
                          {option.value}
                        </option>
                      ))}
                    </select>
                    {meta.error && meta.touched && (
                      <span
                        style={{
                          color: "red",
                          textDecoration: "none",
                          display: "list-item",
                          top: "0px",
                          right: "50px",
                          position: "absolute"
                        }}
                      >
                        {meta.error}
                      </span>
                    )}
                  </div>
                )}
              </Field>

              <Field
                name="Work Licence"
                type="text"
                placeholder="Work Licence"
                // value={props.formData.profile.licence}
                // onChange={e => props.changeInput(e, ["profile", "licence"])}
                validate={required}
                subscription={{
                  value: true,
                  active: true,
                  error: true,
                  touched: true
                }}
              >
                {({ input, meta, placeholder }) => (
                  <div
                    className="form-group addon_input"
                    style={{ position: "relative" }}
                  >
                    <input
                      {...input}
                      className="form-control"
                      placeholder={placeholder}
                    />
                    {meta.error && meta.touched && (
                      <span
                        style={{
                          color: "red",
                          textDecoration: "none",
                          display: "list-item",
                          top: "0px",
                          right: "50px",
                          position: "absolute"
                        }}
                      >
                        {meta.error}
                      </span>
                    )}
                  </div>
                )}
              </Field>

              <Field
                name="Hourly rate"
                type="text"
                placeholder="Hourly rate"
                // value={props.formData.profile.hourlyRate}
                // onChange={e => props.changeInput(e, ["profile", "hourlyRate"])}
                validate={required}
                subscription={{
                  value: true,
                  active: true,
                  error: true,
                  touched: true
                }}
              >
                {({ input, meta, placeholder }) => (
                  <div
                    className="form-group addon_input"
                    style={{ position: "relative" }}
                  >
                    <input
                      {...input}
                      className="form-control"
                      placeholder={placeholder}
                    />
                    {meta.error && meta.touched && (
                      <span
                        style={{
                          color: "red",
                          textDecoration: "none",
                          display: "list-item",
                          top: "0px",
                          right: "50px",
                          position: "absolute"
                        }}
                      >
                        {meta.error}
                      </span>
                    )}
                  </div>
                )}
              </Field>
            </div>
            <div className="col-md-6">
              <div className="partners">
                <div className="row_cus" style={{
                  display: "-webkit-flex",
                }}>
                  <div className="col-md-4">
                    <div className="partner_detail add_member">
                      <input
                        type="file"
                        style={{ display: "none" }}
                        onChange={e => props.imageUpload(e, ["profile", "pic"])}
                        ref={fileInput => (this.fileInput = fileInput)}
                      />
                      <button
                        type="button"
                        className="bn btn-default"
                        onClick={() => this.fileInput.click()}
                      >
                        <img
                          style={{ marginBottom: "0px" }}
                          src={
                            props.formData.profile.pic.length > 0
                              ? URL.createObjectURL(
                                  props.formData.profile.pic[0]
                                )
                              : require("./../../../assests/Images/add_ic.png")
                          }
                        />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default profileForm;
