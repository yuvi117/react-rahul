import React from "react";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { DateUtils } from "react-day-picker";
import dateFnsFormat from "date-fns/format";
import dateFnsParse from "date-fns/parse";

// eslint-disable-next-line
import "!style-loader!css-loader!./ContactForm.css";
// eslint-disable-next-line
import "!style-loader!css-loader!react-day-picker/lib/style.css";

const contactForm = props => {
  const { Field } = props;
  const required = value => (value ? undefined : "Required");
  const pattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
  const dateFormat = value => {
    return pattern.test(value) ? undefined : "Enter a valid date";
  };

  function parseDate(str, format, locale) {
    const parsed = dateFnsParse(str, format, new Date(), { locale });
    if (DateUtils.isDate(parsed)) {
      return parsed;
    }
    return undefined;
  }

  function formatDate(date, format, locale) {
    return dateFnsFormat(date, format, { locale });
  }

  // const _onFocus = e => {
  //   e.currentTarget.type = "date";
  // };
  // const _onBlur = e => {
  //   e.currentTarget.type = "text";
  //   e.currentTarget.placeholder = "Date of Birth";
  // };
  return (
    <section className="contact_form space_100">
      <div className="container">
        <div className="bg_fff">
          <div className="text-center">
            <h2>Contact</h2>
          </div>
          <div className="flex">
            <Field
              name="Name"
              type="text"
              placeholder="Name"
              validate={required}
              subscription={{
                value: true,
                active: true,
                error: true,
                touched: true
              }}
            >
              {({ input, meta, placeholder }) => (
                <div
                  className="form-group mr-4"
                  style={{ position: "relative" }}
                >
                  <input
                    {...input}
                    className="form-control"
                    placeholder={placeholder}
                  />
                  {meta.error && meta.touched && (
                    <span
                      style={{
                        color: "red",
                        textDecoration: "none",
                        display: "list-item",
                        top: "0px",
                        right: "50px",
                        position: "absolute"
                      }}
                    >
                      {meta.error}
                    </span>
                  )}
                </div>
              )}
            </Field>
            <Field
              name="Date of Birth"
              type="text"
              placeholder="DD/MM/YYYY"
              validate={dateFormat}
              subscription={{
                value: true,
                active: true,
                error: true,
                touched: true
              }}
            >
              {({ input, meta, placeholder }) => (
                <div
                  className="form-group mr-4"
                  style={{ position: "relative" }}
                >
                  <DayPickerInput
                  style={{width: '-webkit-fill-available'}}
                    {...input}
                    value={input.value}
                    format="dd/MM/yyyy"
                    placeholder={placeholder}
                    formatDate={formatDate}
                    onBlur={input.onBlur}
                    parseDate={parseDate}
                    onDayChange={(day, val, func) =>
                      input.onChange(func.getInput().value.trim())
                    }
                    inputProps={{ name: input.name, onBlur: input.onBlur }}
                  />
                  {meta.error && meta.touched && (
                    <span
                      style={{
                        color: "red",
                        textDecoration: "none",
                        display: "list-item",
                        top: "0px",
                        right: "50px",
                        position: "absolute"
                      }}
                    >
                      {meta.error}
                    </span>
                  )}
                </div>
              )}
            </Field>
          </div>

          <div className="flex">
            <Field
              name="Last School"
              type="text"
              placeholder="Last School"
              validate={required}
              subscription={{
                value: true,
                active: true,
                error: true,
                touched: true
              }}
            >
              {({ input, meta, placeholder }) => (
                <div
                  className="form-group mr-4"
                  style={{ position: "relative" }}
                >
                  <input
                    {...input}
                    className="form-control"
                    placeholder={placeholder}
                  />
                  {meta.error && meta.touched && (
                    <span
                      style={{
                        color: "red",
                        textDecoration: "none",
                        display: "list-item",
                        top: "0px",
                        right: "50px",
                        position: "absolute"
                      }}
                    >
                      {meta.error}
                    </span>
                  )}
                </div>
              )}
            </Field>
            <Field
              name="Phone Number"
              type="number"
              placeholder="Phone Number"
              validate={required}
              // value={props.formData.contact.phone}
              // onChange={e => props.changeInput(e, ["contact", "phone"])}
              subscription={{
                value: true,
                active: true,
                error: true,
                touched: true
              }}
            >
              {({ input, meta, placeholder }) => (
                <div
                  className="form-group mr-4"
                  style={{ position: "relative" }}
                >
                  <input
                    {...input}
                    className="form-control"
                    placeholder={placeholder}
                  />
                  {meta.error && meta.touched && (
                    <span
                      style={{
                        color: "red",
                        textDecoration: "none",
                        display: "list-item",
                        top: "0px",
                        right: "50px",
                        position: "absolute"
                      }}
                    >
                      {meta.error}
                    </span>
                  )}
                </div>
              )}
            </Field>
          </div>

          <Field
            name="Skills"
            type="text"
            placeholder="Skills"
            validate={required}
            // value={props.formData.contact.skills}
            // onChange={e => props.changeInput(e, ["contact", "skills"])}
            subscription={{
              value: true,
              active: true,
              error: true,
              touched: true
            }}
          >
            {({ input, meta, placeholder }) => (
              <div
                className="form-group pt-2 pb-1"
                style={{ position: "relative" }}
              >
                <input
                  {...input}
                  className="form-control"
                  placeholder={placeholder}
                />
                {meta.error && meta.touched && (
                  <span
                    style={{
                      color: "red",
                      textDecoration: "none",
                      display: "list-item",
                      top: "0px",
                      right: "65px",
                      position: "absolute"
                    }}
                  >
                    {meta.error}
                  </span>
                )}
              </div>
            )}
          </Field>
          <Field
            name="Experience"
            type="text"
            placeholder="Experience"
            validate={required}
            // value={props.formData.contact.experience}
            // onChange={e => props.changeInput(e, ["contact", "experience"])}
            subscription={{
              value: true,
              active: true,
              error: true,
              touched: true
            }}
          >
            {({ input, meta, placeholder }) => (
              <div
                className="form-group pt-2 pb-1"
                style={{ position: "relative" }}
              >
                <input
                  {...input}
                  className="form-control"
                  placeholder={placeholder}
                />
                {meta.error && meta.touched && (
                  <span
                    style={{
                      color: "red",
                      textDecoration: "none",
                      display: "list-item",
                      top: "0px",
                      right: "65px",
                      position: "absolute"
                    }}
                  >
                    {meta.error}
                  </span>
                )}
              </div>
            )}
          </Field>
          <p>Share Your Social Media Contact Information (Optional)</p>
          <div className="social_li">
            <div className="icons_social">
              <a href="#">
                <i className="fa fa-envelope" />
              </a>
              <a href="#">
                <i className="fa fa-facebook-f" />
              </a>
              <a href="#">
                <i className="fa fa-youtube" />
              </a>
              <a href="#">
                <i className="fa fa-whatsapp" />
              </a>
              <a href="#">
                <i className="fa fa-instagram" />
              </a>
              <a href="#">
                <i className="fa fa-slack" />
              </a>
              <a href="#">
                <i className="fa fa-twitter" />
              </a>
            </div>
          </div>

          <Field
            name="About"
            type="text"
            placeholder="Write up to five descriptive words about you"
            // value={props.formData.contact.about}
            // onChange={e => props.changeInput(e, ["contact", "about"])}
            subscription={{
              value: true,
              active: true,
              error: true,
              touched: true
            }}
          >
            {({ input, meta, placeholder }) => (
              <div
                className="form-group pt-2 pb-1"
                style={{ position: "relative" }}
              >
                <input
                  {...input}
                  className="form-control"
                  placeholder={placeholder}
                />
                {meta.error && meta.touched && (
                  <span
                    style={{
                      color: "red",
                      textDecoration: "none",
                      display: "list-item",
                      top: "0px",
                      right: "65px",
                      position: "absolute"
                    }}
                  >
                    {meta.error}
                  </span>
                )}
              </div>
            )}
          </Field>
        </div>
      </div>
    </section>
  );
};

export default contactForm;
