import React from "react";

// eslint-disable-next-line
import "!style-loader!css-loader!./../ContactForm/ContactForm.css";

const personalityForm = props => {
  let values = {
    PQ1: false,
    PQ2: false,
    PQ3: false,
    PQ4: false,
    PQ5: false,
    PQ6: false,
    PQ7: false,
    PQ8: false
  };
  const { Field } = props;
  const required = name => (values[name] ? undefined : "Required");
  return (
    <section className="personality">
      <div className="container">
        <div className="bg_fff">
          <div className="text-center">
            <h2>Personality</h2>
          </div>
          <ol>
            {[
              { name: "PQ1", question: "I Prefer to work with others" },
              {
                name: "PQ2",
                question:
                  "I Prefer to have only a few close friends,rather then a great number of friends"
              },
              {
                name: "PQ3",
                question:
                  "I Have a hard time starting a conversation with people at the begining of meeting"
              },
              {
                name: "PQ4",
                question: "Relationships are essential for productive workspace"
              },
              { name: "PQ5", question: "I get sad when other feel sad" },
              {
                name: "PQ6",
                question: "I always finish what i started before taking a break"
              },
              { name: "PQ7", question: "I tend to be on time" },
              {
                name: "PQ8",
                question: "Others see me as organized and methodical"
              }
            ].map(item => (
              <li key={item.name}>
                <p>{item.question}</p>
                <div className="flex">
                  {[
                    { id: 1, value: "0", lable: "Agree" },
                    { id: 2, value: "1", lable: "Disagree" }
                  ].map(option => (
                    <Field
                      key={option.id}
                      name={item.name}
                      type="radio"
                      value={option.value}
                      validate={v => required(item.name)}
                      subscription={{
                        value: true,
                        active: true,
                        error: true,
                        touched: true
                      }}
                    >
                      {({ input, meta }) => (
                        <div className={option.id === 1 ? "spc_rght" : null}>
                          <input
                            {...input}
                            onChange={e => {
                              values[e.target.name] = e.target.checked;

                              return input.onChange(e);
                            }}
                          />
                          &nbsp;&nbsp;{option.lable}
                        </div>
                      )}
                    </Field>
                  ))}
                </div>
              </li>
            ))}
          </ol>
        </div>
      </div>
    </section>
  );
};

export default personalityForm;
