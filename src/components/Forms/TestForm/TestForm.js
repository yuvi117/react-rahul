import React from "react";

// eslint-disable-next-line
import "!style-loader!css-loader!./../ContactForm/ContactForm.css";

const testForm = props => {
  let values = {
    T1: false,
    T2: false,
    t1: null
  };
  const { Field } = props;
  const required = name => (values[name] ? undefined : "Required");
  const onChangeHandler = (e, input) => {
    if (e.target.checked) {
      values[input.name] && values[input.name].length > 0
        ? values[input.name].push(e.target.value)
        : (values[input.name] = [e.target.value]);
    } else {
      values[input.name].length > 1
        ? values[input.name].splice(
            values[input.name].indexOf(e.target.value),
            1
          )
        : (values[input.name] = null);
    }

    console.log("values", values);
    console.log("e.target.value", e.target.checked);
    // e.target.value = values[input.name] ? values[input.name].join(",") : "";
    console.log("eche", e.target.checked);
    console.log("evalue", e.target.value);
    let ref = document.getElementById("t1");
    let lastValue = ref.value;
    let event = new Event("change", { bubbles: true });
    event.simulated = true;
    ref.value = values[input.name] ? values[input.name].join(",") : "";
    ref.defaultValue = values[input.name] ? values[input.name].join(",") : "";
    console.log("this.textInput", ref.value);
    let tracker = ref._valueTracker;
    if (tracker) {
      tracker.setValue(lastValue);
    }
    return ref.dispatchEvent(event);
  };

  // const valueChangeHandler = () => {
  //   console.log("valueChangeHandler");
  // };
  return (
    <section className="test space_100">
      <div className="container">
        <div 
        className="row_cus"
         style={{
          display: "-webkit-flex",
        }}>
          {[
            {
              id: 1,
              statements: [
                "George runs faster then Tim ",
                "Tim runs faster then Lucas",
                "George runs faster then Lucas"
              ],
              question:
                "If the first two statement is true, the third statement is",
              name: "T1",
              options: [{ id: "1", value: "True" }, { id: "2", value: "False" }]
            },
            {
              id: 2,
              statements: [
                "The Diagram below show AND gates that take two inputs (from the left), Each produce one output (to the right).",
                "AND gates will output 1 if both inputs are 1, otherwise that will output 0. (You can read “1”as equivalent to true and 0 as equivalent to false)",
                "George runs faster then Lucas"
              ],
              question: "What will the output be at question mark ?",
              name: "T2",
              options: [{ id: "1", value: "0" }, { id: "2", value: "1" }]
            }
          ].map(item => (
            <div className="col-md-6 col-sm-12" key={item.id}>
              <div className="bg_fff">
                <div className="text-center">
                  <h2>Test{item.id}</h2>
                </div>
                <ol>
                  {item.statements.map((statement, index) => (
                    <li key={index}>{statement}</li>
                  ))}
                  <p>{item.question}</p>
                  <div className="flex">
                    {item.options.map(option => (
                      <Field
                        key={option.id}
                        name={item.name}
                        type="radio"
                        value={option.value}
                        validate={v => required(item.name)}
                        subscription={{
                          value: true,
                          active: true,
                          error: true,
                          touched: true
                        }}
                      >
                        {({ input, meta }) => (
                          <div
                            className={option.id === "1" ? "spc_rght" : null}
                          >
                            <input
                              {...input}
                              onChange={e => {
                                console.log("valuesdow", values);

                                values[e.target.name] = e.target.checked;

                                return input.onChange(e);
                              }}
                            />
                            &nbsp;&nbsp;{option.value}
                          </div>
                        )}
                      </Field>
                    ))}
                  </div>
                </ol>
              </div>
            </div>
          ))}
        </div>
        <div 
        className="row_cus" 
        style={{
          display: "-webkit-flex",
        }}>
          <Field
            name="t1"
            type="text"
            value={values["t1"] || ""}
            validate={v => required("t1")}
            subscription={{
              value: true,
              active: true,
              error: true,
              touched: true
            }}
          >
            {({ input, meta }) => (
              <div className="col-md-6 col-sm-12">
                <div className="bg_fff">
                  <div className="text-center">
                    <h2>Test3</h2>
                  </div>
                  <ol>
                    <li>
                      Assume that you a playing a Tic tac toc game. You are
                      playing as X while you opponent plays as O.
                    </li>
                    <li>The game is played as follow</li>
                    <p>
                      Find all the possible grid tiles where you can put as X
                      that will help you guarantee yourself a win
                    </p>
                    <div className="flex">
                      <div className="spc_rght test_radio_btn">
                        <input
                          {...input}
                          id="t1"
                          style={{ display: "none" }}
                          // onChange={valueChangeHandler}
                          ref={textInput => (this.textInput = textInput)}
                        />
                        <input
                          type="checkbox"
                          name="{input.name}"
                          value="A"
                          onChange={e => onChangeHandler(e, input)}
                        />
                        &nbsp;&nbsp;A&nbsp;&nbsp;
                        <input
                          type="checkbox"
                          name="{input.name}"
                          value="B"
                          onChange={e => onChangeHandler(e, input)}
                        />
                        &nbsp;&nbsp;B&nbsp;&nbsp;
                        <input
                          type="checkbox"
                          name="{input.name}"
                          value="C"
                          onChange={e => onChangeHandler(e, input)}
                        />
                        &nbsp;&nbsp;C&nbsp;&nbsp;
                        <input
                          type="checkbox"
                          name="{input.name}"
                          value="D"
                          onChange={e => onChangeHandler(e, input)}
                        />
                        &nbsp;&nbsp;D&nbsp;&nbsp;
                        <input
                          type="checkbox"
                          name="{input.name}"
                          value="E"
                          onChange={e => onChangeHandler(e, input)}
                        />
                        &nbsp;&nbsp;E&nbsp;&nbsp;
                        <input
                          type="checkbox"
                          name="{input.name}"
                          value="F"
                          onChange={e => onChangeHandler(e, input)}
                        />
                        &nbsp;&nbsp;F&nbsp;&nbsp;
                      </div>
                    </div>
                  </ol>
                </div>
              </div>
            )}
          </Field>
        </div>
        <div className="save text-center">
          <button type="submit" className="btn btn-default">
            Save
          </button>
        </div>
      </div>
    </section>
  );
};

export default testForm;
