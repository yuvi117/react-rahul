import React from "react";

// eslint-disable-next-line
import "!style-loader!css-loader!./../../ContactForm/ContactForm.css";

const available = props => {
  let values = {
    A1: false,
    A2: false
  };
  const { Field } = props;
  const required = value =>
    Object.values(values).find(val => val === true) ? undefined : "Required";
  return (
    <div className="col-md-4 col-sm-6">
      <div className="avaliable_inner">
        <ul>
          {[
            { id: "A1", lable: "Full Time" },
            { id: "A2", lable: "Morning" },
            { id: "A3", lable: "Evening" },
            { id: "A4", lable: "Night" }
          ].map(item => (
            <Field
              key={item.id}
              name={item.id}
              type="checkbox"
              validate={required}
              subscription={{
                value: true,
                active: true,
                error: true,
                touched: true
              }}
            >
              {({ input, meta }) => (
                <li>
                  <p>{item.lable}</p>
                  <label className="switch">
                    <input {...input} className="form-control" onChange={(e) => {
                      values[e.target.name] = e.target.checked
                      return input.onChange(e)
                    }} />
                    <span className="slider round" />
                  </label>
                  {/*meta.error && meta.touched && (
                    <span
                      style={{
                        color: "red",
                        textDecoration: "none",
                        display: "list-item",
                        top: "0px",
                        right: "50px",
                        position: "absolute"
                      }}
                    >
                      {meta.error}
                    </span>
                    )*/}
                </li>
              )}
            </Field>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default available;
