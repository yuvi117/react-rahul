import React from "react";

// eslint-disable-next-line
import "!style-loader!css-loader!./../ContactForm/ContactForm.css";
import Available from "./Available/Available";
import BlockDays from "./BlockDays/BlockDays";
import Calender from "./Calender/Calender";

const avalabilityForm = props => (
  <section className="available space_100">
    <div className="container">
      <div className="bg_fff">
        <div className="text-center">
          <h2>Availability</h2>
        </div>
        <div 
          className="row_cus"
         style={{
          display: "-webkit-flex",
        }}>
          <Available Field={props.Field} />
          <BlockDays Field={props.Field} />
          <Calender Field={props.Field} />
        </div>
      </div>
    </div>
  </section>
);

export default avalabilityForm;
