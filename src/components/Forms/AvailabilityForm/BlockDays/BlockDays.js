import React from "react";
import Checkbox from "@material-ui/core/Checkbox";

// eslint-disable-next-line
import "!style-loader!css-loader!./../../ContactForm/ContactForm.css";

const blockDays = props => {
  let values = {
    MO: false,
    TU: false,
    WE: false,
    TH: false,
    FR: false,
    SA: false
  };
  const { Field } = props;
  const required = value =>
    Object.values(values).find(val => val === true) ? undefined : "Required";
  return (
    <div className="col-md-4 col-sm-6">
      <div className="avaliable_inner blockdays">
        <h6 className="text-center">Block days of Week</h6>
        <ul>
          {[
            { id: "MO", lable: "Mondays" },
            { id: "TU", lable: "Tuesdays" },
            { id: "WE", lable: "Wednesdays" },
            { id: "TH", lable: "Thursdays" },
            { id: "FR", lable: "Fridays" },
            { id: "SA", lable: "Saturdays" }
          ].map(item => (
            <Field
              key={item.id}
              name={item.id}
              type="checkbox"
              validate={required}
              subscription={{
                value: true,
                active: true,
                error: true,
                touched: true
              }}
            >
              {({ input, meta }) => (
                <li style={{position: 'relative'}}>
                  <p>{item.lable}</p>
                  <Checkbox
                    {...input}
                    style={{position: 'absolute', right: '0'}}
                    onChange={e => {
                      values[e.target.name] = e.target.checked;
                      return input.onChange(e);
                    }}
                  />
                  {/*meta.error && meta.touched && (
                    <span
                      style={{
                        color: "red",
                        textDecoration: "none",
                        display: "list-item",
                        top: "0px",
                        right: "50px",
                        position: "absolute"
                      }}
                    >
                      {meta.error}
                    </span>
                    )*/}
                </li>
              )}
            </Field>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default blockDays;
