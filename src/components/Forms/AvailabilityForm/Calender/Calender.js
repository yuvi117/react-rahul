import React from "react";
import Checkbox from "@material-ui/core/Checkbox";

// eslint-disable-next-line
("!style-loader!css-loader!./../../ContactForm/ContactForm.css");

const calender = props => {
  let values = {
    AVA: false,
    BLO: false
  };
  const { Field } = props;
  const required = value =>
    Object.values(values).find(val => val === true) ? undefined : "Required";
  return (
    <div className="col-md-4 col-sm-6">
      <div className="avaliable_inner calender">
        <h6 className="text-center">Calender</h6>
        {[{ id: "AVA", lable: "Available days" }, { id: "BLO", lable: "Blocked days" }].map(
          item => (
            <Field
              key={item.id}
              name={item.id}
              type="checkbox"
              validate={required}
              subscription={{
                value: true,
                active: true,
                error: true,
                touched: true
              }}
            >
              {({ input, meta }) => (
                <div>
                  <Checkbox
                    {...input}
                    onChange={e => {
                      values[e.target.name] = e.target.checked;
                      return input.onChange(e);
                    }}
                  />
                  &nbsp; {item.lable}
                  <br />
                  {/*meta.error && meta.touched && (
                  <span
                    style={{
                      color: "red",
                      textDecoration: "none",
                      display: "list-item",
                      top: "0px",
                      right: "50px",
                      position: "absolute"
                    }}
                  >
                    {meta.error}
                  </span>
                  )*/}
                </div>
              )}
            </Field>
          )
        )}
      </div>
    </div>
  );
};

export default calender;
