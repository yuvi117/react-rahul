import React from 'react'

import NavigationItems from '../NavigationItems/NavigationItems'
import Aux from '../../../hoc/Auxx/Auxx'

const sideDrawer = props => {
  // let attachedClasses = [classes.SideDrawer, classes.Close];
  // if (props.open) {
  //     attachedClasses = [classes.SideDrawer, classes.Open];
  // }<img
  //   src={osiLogo}
  //   alt='OSI'
  //   style={{
  //     position: 'absolute',
  //     top: '14px',
  //     left: '5px',
  //     width: '51px',
  //     heigth: '50px'
  //   }}
  // />
  return (
    <Aux>
      <div>
        <nav>
          <NavigationItems
            isAuthenticated={props.isAuth}
            clicked={props.drawerToggleClicked}
            drawerOpen={props.opened}
            drawerClose={props.closed}
            open={props.open}
            hovered={props.drawerToggleHovered}
            sideMenu={props.hover}
          />
        </nav>
      </div>
    </Aux>
  )
}

export default sideDrawer
