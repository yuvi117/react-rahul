import React from 'react'
import { NavLink } from 'react-router-dom'

import classes from './NavigationItems.css'
import travelLogo from './../../../assests/Images/ic_1.png'
import networkLogo from './../../../assests/Images/ic_2.png'
import hireLogo from './../../../assests/Images/ic_3.png'
import NavigationBar from '../NavigationBar/NavigationBar'
import BookTourForm from '../../../containers/Main/BookTour/BookTourForm/BookTourForm'

const navigationItems = props => {
  return (
    <div className={classes[props.classN]}>
      <section
        className={classes.main_sec_1}
        style={{
          backgroundImage:
            'url(' +
            require(`./../../../assests/Images/${props.image ||
              'sunset.jpg'}`) +
            ')'
        }}
      >
        {props.authenticated ? (
          <NavigationBar
            notif={props.notifications}
            clicked={props.onClick}
            currentItem={props.currentNav}
            backButton={props.backButton}
          />
        ) : null}
        <div className={classes.logo_ics}>
          <div className='container'>
            <ul>
              {[
                { id: 1, image: travelLogo, link: '/BookTour', class: 'text-right' },
                { id: 2, image: networkLogo, link: '/Networks', class: 'text-center' },
                { id: 3, image: hireLogo, link: '/celesteHiring', class: 'text-left' }
              ].map(logo => {
                return (
                  <li key={logo.id} className={logo.class}>
                    <NavLink to={logo.link}>
                      <img src={logo.image} />
                    </NavLink>
                  </li>
                )
              })}
            </ul>
          </div>
        </div>

        <div className={classes.bg_nav}>
          <div className='container'>
            <ul style={{color: "white"}}>
              <li className='text-center' >
                Travel.
              </li>
              <li className='text-center'>
                Network.
              </li>
              <li className='text-center'>
                Hire.
              </li>
            </ul>
          </div>
        </div>
        <div className={`${classes.btn_work} text-center`}>
          <button type='submit' className={`${classes.btn} btn-default`}>
            How does it work
          </button>
        </div>
        {props.showTourForm ? <BookTourForm /> : null}
      </section>
    </div>
  )
}

export default navigationItems
