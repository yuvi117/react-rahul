import React from "react";
import { NavLink } from "react-router-dom";

import classes from "./NavigationBar.css";
import { routes } from "../../../shared/constants";

const navigationBar = props => {
  const navBar = [
    {
      id: routes.HOME,
      image: "home.png",
      link: "/Home",
      route: routes.HOME,
      notification: props.notif.home || 0
    },
    {
      id: routes.BOOK_TOUR,
      image: "Vedi-copy.png",
      link: "/BookTour",
      route: routes.BOOK_TOUR,
      notification: props.notif.bookTour || 0
    },
    {
      id: routes.NETWORKS,
      image: "logo.png",
      link: "/Networks",
      route: routes.NETWORKS,
      notification: props.notif.network || 0
    },
    {
      id: routes.CELESTE_HIRING,
      image: "Celeste_  wt.png",
      link: "/celesteHiring",
      route: routes.CELESTE_HIRING,
      notification: props.notif.celesteHiring || 0
    },
    {
      id: 7,
      image: "heart.png",
      link: "/Home",
      route: routes.HOME,
      notification: 0
    },
    {
      id: 8,
      image: "strategy.png",
      link: "/Home",
      route: routes.HOME,
      notification: 0
    },
    {
      id: 9,
      image: "share.png",
      link: "/Home",
      route: routes.HOME,
      notification: 0
    },
    { id: 10, image: "user.png", link: "", route: false, notification: 0 }
  ];

  return (
    <nav className={`navbar navbar-expand-md ${classes.navbarDark}`}>
      <div className="container-fluid">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#collapsibleNavbar"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div
          className="collapse navbar-collapse"
          id="collapsibleNavbar"
          style={{ paddingTop: "10px" }}
        >
          <ul className={classes.navbar_nav}>
            <li className="nav-item">
              <span
                className="nav-link"
                style={{ cursor: "pointer" }}
                onClick={props.backButton}
              >
                <img
                  className={`${classes.back} ${classes.opacity_img}`}
                  src={require("./../../../assests/Images/back.png")}
                />
              </span>
            </li>
          </ul>

          <ul className={`${classes.navbar_nav} ${classes.right_nav}`}>
            {navBar.map((navItem, index) => {
              let classname = ["nav-item"];
              if (props.currentItem == navItem.id) {
                classname = ["nav-item", classes.active];
              }
              return (
                <li
                  key={navItem.id}
                  className={classname.join(" ")}
                  onClick={() => props.clicked(navItem.id, navItem.route)}
                >
                  <NavLink
                    to={navItem.link}
                    exact
                    onClick={e => (navItem.link ? null : e.preventDefault())}
                  >
                    <span className="nav-link">
                      {navItem.notification ? (
                        <img
                          style={{
                            position: "absolute",
                            right: "29%",
                            bottom: "53%",
                            zIndex: "1"
                          }}
                          src={require(`./../../../assests/Images/circle.png`)}
                        />
                      ) : null}
                      <img
                        className={classes.opacity_img}
                        src={require(`./../../../assests/Images/${navItem.image}`)}
                      />
                    </span>
                  </NavLink>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default navigationBar;
