import React from "react";

import classes from "./Footer.css";

const footer = props => (
  <section className={`${classes.adv_category} ${classes.space_60}`}>
    <div className="container">
      <div
        className={classes.row_cus}
        style={{
          display: "-webkit-flex",
        }}
      >
        <div className="col-md-2 col-sm-6">
          <div className={`${classes.adv_6} text-center`}>
            <a href="http://google.com" target="_blank">
              Business Tours
            </a>
          </div>
        </div>

        <div className="col-md-2 col-sm-6">
          <div className={`${classes.adv_6} ${classes.bg_adv} text-center`}>
            <a href="http://google.com" target="_blank">
              AM Builds
            </a>
          </div>
        </div>

        <div className="col-md-2 col-sm-6">
          <div className={`${classes.adv_6} text-center`}>
            <a href="http://google.com" target="_blank">
              Coliseum
            </a>
          </div>
        </div>

        <div className="col-md-2 col-sm-6">
          <div className={`${classes.adv_6} ${classes.bg_adv} text-center`}>
            <a href="http://google.com" target="_blank">
              Bologistics
            </a>
          </div>
        </div>

        <div className="col-md-2 col-sm-6">
          <div className={`${classes.adv_6} text-center`}>
            <a href="http://google.com" target="_blank">
              VEDI Ex
            </a>
          </div>
        </div>

        <div className="col-md-2 col-sm-6">
          <div className={`${classes.adv_6} ${classes.bg_adv} text-center`}>
            <a href="http://google.com" target="_blank">
              ICI
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default footer;
