import React from 'react'

import classes from './Survey.css'
import SurveyOption from './SurveyOption/SurveyOption'

const survey = props => (
  <section
    id='survey_1'
    className={`${classes.survey_sec} ${classes.space_100}`}
  >
    <div className='container'>
      <div className={`${classes.survey_div} ${classes.bg_white}`}>
        <h1>{props.surveyData.question}</h1>

        <div 
        className={classes.row_cus}
         style={{
          display: "-webkit-flex",
          
        }}>
          {props.surveyData.options.map((option, index) => {
            let active = false;
            if (option.id === props.selectedSurveyOption) {
              active=true
            }
            return <SurveyOption key={option.id} active={active} clicked={() => props.selectSurveyOption(option.id, option.name)} option={option} />
          })}
        </div>

        <div className={`${classes.next_btn} text-center ${classes.pb5}`}>
          <button disabled={props.selectedSurveyOption === 0}
            className={`${classes.btn} btn-default`}
            onClick={props.changeSurvey}
          >
            {props.surveyLength === props.currentSurvey ? 'Done' : 'Next'}
          </button>
        </div>

        <div className={`${classes.steps_round} text-center`}>
          <div className={classes.center}>
            {Array.apply(null, Array(props.surveyLength)).map(
              (value, index) => {
                let classname = ['fa fa-circle']
                if (props.currentSurvey === index+1) {
                  classname = ['fa fa-circle', classes.active]
                }
                return <i key={index} className={classname.join(' ')} style={{padding: '3px'}} />
              }
            )}
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default survey
