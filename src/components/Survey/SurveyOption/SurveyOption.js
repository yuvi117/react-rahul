import React from 'react';

import classes from './SurveyOption.css';

const surveyOption = (props) => (
    <div className={`col-md-4 col-sm-6 ${classes.p5}`}>
                      <a><div className={`${classes.option_bg} ${props.active ? classes.activeSurvey : null} text-center`} onClick={props.clicked}>
                        <img src={require(`./../../../assests/Images/${props.option.imageName}`)} alt='Survey'/>
                      </div></a>
                  </div>
                  
);

export default surveyOption;